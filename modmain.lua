PrefabFiles = {
    "wx",
    "wx_placer",
    "wxdiviningrod",
    "wxdiviningrodbase",
    "wxdiviningrodbase_placer",
    "wilsondummy",
    "wilsondummy_placer",
}

Assets = 
{
    Asset("ATLAS", "images/avatars.xml"),
    Asset("ATLAS", "images/inventoryimages.xml"),
}

local RECIPETABS = GLOBAL.RECIPETABS
local Recipe = GLOBAL.Recipe
local Ingredient = GLOBAL.Ingredient
local TECH = GLOBAL.TECH
local STRINGS = GLOBAL.STRINGS
local Ents = GLOBAL.Ents
local TheWorld = GLOBAL.TheWorld

-- Build Recipe --------------------------

AddRecipe("wx",
    {
        Ingredient("gears", 9),
        Ingredient("transistor", 2),
        Ingredient("trinket_6", 2)
    },
    RECIPETABS.SCIENCE,
    TECH.SCIENCE_TWO,
    "wx_placer",
    nil, nil, nil,
    "wxbuilder",
    "images/avatars.xml",
    "avatar_WX78.tex"
)
AddRecipe("wxdiviningrod",
    {
        Ingredient("twigs", 1),
        Ingredient("nightmarefuel", 4),
        Ingredient("gears", 1)
    },
    RECIPETABS.SCIENCE,
    TECH.SCIENCE_TWO,
    nil,
    nil, nil, nil,
    "wxbuilder",
    "images/inventoryimages.xml",
    "diviningrod.tex"
)
AddRecipe("wxdiviningrodbase",
    {
        Ingredient("boards", 1)
    },
    RECIPETABS.SCIENCE,
    TECH.NONE,
    "wxdiviningrodbase_placer",
    nil, nil, nil,
    "wxbuilder",
    "images/inventoryimages.xml",
    "teleportato_base.tex"
)
AddRecipe("wilsondummy",
    {
        Ingredient("boards", 4),
        Ingredient("cookedmeat", 4),
        Ingredient("beardhair", 4),
    },
    RECIPETABS.SCIENCE,
    TECH.NONE,
    "wilsondummy_placer",
    nil, nil, nil,
    nil,
    "images/inventoryimages.xml",
    "resurrectionstatue.tex"
)

-- Character Speech ----------------------

STRINGS.NAMES.WX = "WX-type Automaton"
STRINGS.RECIPE_DESC.WX = "A brand-new automaton."
STRINGS.NAMES.WXDIVININGROD = "Voxola PR-76"
STRINGS.RECIPE_DESC.WXDIVININGROD = "Portable signal trasmitter."
STRINGS.NAMES.WXDIVININGRODBASE = "PR-76 Plateform"
STRINGS.RECIPE_DESC.WXDIVININGRODBASE = "Socket for a trasmitter."
STRINGS.NAMES.WILSONDUMMY = "Practice Dummy"
STRINGS.RECIPE_DESC.WILSONDUMMY = "Good target for training."

STRINGS.CHARACTERS.GENERIC.DESCRIBE.WX = "The crown jewel of industry."
STRINGS.CHARACTERS.WILLOW.DESCRIBE.WX = "It's a pity that your shell hardly catches fire."
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.WX = "Tiny robot wants to be Wolfgang's friend."
STRINGS.CHARACTERS.WENDY.DESCRIBE.WX = "Do you feel hollow inside?"
STRINGS.CHARACTERS.WX78.DESCRIBE.WX = "ONE OF MY DEAR SIBLINGS"
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.WX = "It looks like the prototype of WX-78."
STRINGS.CHARACTERS.WOODIE.DESCRIBE.WX = "You can use an axe. Great."
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.WX = "Its design is quite different from my clockwork attendants."
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.WX = "Follow close behind, soldier!"
STRINGS.CHARACTERS.WEBBER.DESCRIBE.WX = "You look more non-organic than yesterday!"
STRINGS.CHARACTERS.WINONA.DESCRIBE.WX = "Best workmate ya can ever count on!"

STRINGS.CHARACTERS.GENERIC.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.GENERIC.DESCRIBE.DIVININGROD
STRINGS.CHARACTERS.WILLOW.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.WILLOW.DESCRIBE.DIVININGROD
STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.WOLFGANG.DESCRIBE.DIVININGROD
STRINGS.CHARACTERS.WENDY.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.WENDY.DESCRIBE.DIVININGROD
STRINGS.CHARACTERS.WX78.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.WX78.DESCRIBE.DIVININGROD
STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.WICKERBOTTOM.DESCRIBE.DIVININGROD
STRINGS.CHARACTERS.WOODIE.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.WOODIE.DESCRIBE.DIVININGROD
STRINGS.CHARACTERS.WAXWELL.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.WAXWELL.DESCRIBE.DIVININGROD
STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.WATHGRITHR.DESCRIBE.DIVININGROD
STRINGS.CHARACTERS.WEBBER.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.WEBBER.DESCRIBE.DIVININGROD
STRINGS.CHARACTERS.WINONA.DESCRIBE.WXDIVININGROD = STRINGS.CHARACTERS.WINONA.DESCRIBE.DIVININGROD

STRINGS.CHARACTERS.GENERIC.DESCRIBE.WXDIVININGRODBASE = "This socket fits the rod."
STRINGS.CHARACTERS.WX78.DESCRIBE.WXDIVININGRODBASE = "PLAY WITH ME"

STRINGS.CHARACTERS.GENERIC.DESCRIBE.WILSONDUMMY = "Arizona would like to try."
STRINGS.CHARACTERS.WX78.DESCRIBE.WILSONDUMMY = "MAKE FURRY MEAT STACK REMEMBER ITS TARGET"

STRINGS.CHARACTERS.WX = STRINGS.CHARACTERS.WX78
STRINGS.CHARACTERS.WX.DESCRIBE = STRINGS.CHARACTERS.WX78.DESCRIBE

-- Players -------------------------------

AddPlayerPostInit(function(inst)
    local build_restriction = GetModConfigData("BUILD_RESTRICTION")
    if build_restriction == "all" then
        inst:AddTag("wxbuilder")
    elseif build_restriction == "wx" and inst.prefab == "wx78" then
        inst:AddTag("wxbuilder")
    elseif build_restriction == "engineers" and (inst.prefab == "winona" or inst.prefab == "wanda") then
        inst:AddTag("wxbuilder")
    elseif build_restriction == "scientists" and (inst.prefab == "wilson" or inst.prefab == "wickerbottom") then
        inst:AddTag("wxbuilder")
    end
end)

-- Prefabs -------------------------------

AddPrefabPostInit("world", function(inst)
    if not GLOBAL.TheWorld.ismastersim then
		return inst
	end

    inst.sentryward = {}
    inst.wxdiviningrodbase = {}
end)

AddPrefabPostInit("sentryward", function(inst)
    if not GLOBAL.TheWorld.ismastersim then
		return inst
	end

    local attr = {}
    attr.ent = inst
    attr.state = nil
    attr.lasttime = GLOBAL.TheWorld.components.worldstate.data.cycles
    attr.load = 1
    table.insert(GLOBAL.TheWorld.sentryward, attr)

    local function GrowPlants(inst)
        local x, y, z = inst.Transform:GetWorldPosition()
        local producerList = TheSim:FindEntities(x, y, z, 20, nil, nil, { "plant" })
        for k, v in pairs(producerList) do
            if v.components.growable ~= nil then
                v.components.growable:OnEntityWake()
            end
        end
    end

    if inst.task ~= nil then
        inst.task:Cancel()
        inst.task = nil
    end
    inst.task = inst:DoPeriodicTask(60, GrowPlants, 0)
end)

AddPrefabPostInit("wxdiviningrodbase", function(inst)
    if not GLOBAL.TheWorld.ismastersim then
		return inst
	end

    table.insert(GLOBAL.TheWorld.wxdiviningrodbase, inst)
end)

AddPrefabPostInit("gears", function(inst)
    if not GLOBAL.TheWorld.ismastersim then
		return inst
	end

    if inst.components.repairer == nil then
        inst:AddComponent("repairer")
    end
    inst.components.repairer.healthrepairvalue = GLOBAL.TUNING.HEALING_HUGE
end)

AddPrefabPostInit("sewing_tape", function(inst)
    if not GLOBAL.TheWorld.ismastersim then
		return inst
	end

    if inst.components.repairer == nil then
        inst:AddComponent("repairer")
        inst.components.repairer.repairmaterial = GLOBAL.MATERIALS.GEARS
    end
    inst.components.repairer.healthrepairvalue = GLOBAL.TUNING.HEALING_MEDSMALL
end)

-- Components ----------------------------

AddComponentPostInit("container", function(self)
    local WXOnSave = self.OnSave
    self.OnSave = function(self)
        local data, references = WXOnSave(self)
        if self.inst:HasTag("wx") then
            data = {items= {}}
            references = {}
        end
        return data, references
    end
end)

AddComponentPostInit("finiteuses", function(self)
    local function PercentChanged(inst, data)
        if inst.components.finiteuses ~= nil and
            data.percent ~= nil and
            data.percent <= 0 and
            inst.components.inventoryitem ~= nil and
            inst.components.inventoryitem.owner ~= nil then
            inst.components.inventoryitem.owner:PushEvent("weaponbroke", { weapon = inst })
        end
    end

    self.inst:ListenForEvent("percentusedchange", PercentChanged)
end)

AddComponentPostInit("fueled", function(self)
    local WXStartConsuming = self.StartConsuming
    self.StartConsuming = function(self)
        WXStartConsuming(self)
        if self.inst.components.equippable ~= nil and self.inst.components.inventoryitem ~= nil and
            self.inst.components.inventoryitem.owner ~= nil and self.inst.components.inventoryitem.owner:HasTag("wx") then
            self.StopConsuming(self)
        end
    end
end)

AddComponentPostInit("teleporter", function(self)
    local function RemoveFollower(doer, wxfollowerList)
        if doer.components.leader ~= nil then
            for follower, v in pairs(doer.components.leader.followers) do
                if follower:HasTag("wx") then
                    table.insert(wxfollowerList, follower)
                    doer.components.leader:RemoveFollower(follower)
                end
            end
        end
    end

    local function AddFollower(doer, wxfollowerList)
        if doer.components.leader ~= nil then
            for k, v in pairs(wxfollowerList) do
                doer.components.leader:AddFollower(v)
            end
        end
    end

    local WXActivate = self.Activate
    self.Activate = function(self, doer)
        local wxfollowerList = {}
        RemoveFollower(doer, wxfollowerList)
        local activate_return = WXActivate(self, doer)
        AddFollower(doer, wxfollowerList)
        return activate_return
    end
end)

--TheInput:GetWorldEntityUnderMouse().components.harvestable.produce = 6
AddComponentPostInit("childspawner", function(self)
    local WXReleaseAllChildren = self.ReleaseAllChildren
    self.ReleaseAllChildren = function(self, target, prefab)
        local children_released = WXReleaseAllChildren(self, target, prefab)
        if target ~= nil and target:HasTag("wx") and self.inst:HasTag("playerowned") then
            for k, v in pairs(children_released) do
                if v.components.combat ~= nil then
                    v.components.combat:DropTarget()
                end
            end
        end
        return children_released
    end
end)

-- Actions -------------------------------

local LOAD = AddAction("LOAD", "AQUIRE CARGO", function(act)
    if act.target.components.container ~= nil and
        act.invobject.components.inventoryitem ~= nil and
        act.doer.components.inventory ~= nil then
        local item = act.invobject.components.inventoryitem:RemoveFromOwner(act.target.components.container.acceptsstacks)
        if item ~= nil then
            -- When a chest is opened by WX, transporting items from the chest CANNOT success.
            --act.target.components.container:Open(act.doer)
            if not act.doer.components.inventory:GiveItem(item) then
                act.target.components.container:GiveItem(item)
                return false
            end
            return true
        end
    end
end)

local AUGMENT = AddAction("AUGMENT", "INSTALL BACKPACK", function(act)
    if act.doer.components.inventory ~= nil then
        local backpack = act.doer.components.inventory:GetEquippedItem(GLOBAL.EQUIPSLOTS.BODY)
        if backpack ~= nil then
            act.doer.components.inventory:DropItem(backpack, true, true)
        end
        if act.target ~= nil and act.target.components.equippable ~= nil and act.target.components.container ~= nil then
            act.doer.components.inventory:Equip(act.target)
            return true
        end
    end
end)

local ASSEMBLE = AddAction("ASSEMBLE", "ASSEMBLE TACKLE", function(act)
    -- target: tacklecontainer, invobject: tackle, doer: wx
    local rod = act.doer.components.inventory:GetEquippedItem(GLOBAL.EQUIPSLOTS.HANDS)
    if act.target.components.container ~= nil and act.invobject.components.inventoryitem ~= nil and
        rod.components.oceanfishingrod ~= nil and rod.components.container ~= nil and not rod.components.container:IsFull() then
        local item = act.invobject.components.inventoryitem:RemoveFromOwner()
        if not rod.components.container:GiveItem(item) then
            act.target.components.container:GiveItem(item)
            return false
        end
        return true
    end
end)

local SELFREPAIR = AddAction("SELFREPAIR", "SELF REPAIR", function(act)
    act.doer.components.health:DoDelta(GLOBAL.TUNING.HEALING_MEDSMALL, nil, nil, nil, nil, true)
    act.doer.SoundEmitter:PlaySound("dontstarve/characters/wx78/levelup")
    return true
end)

-- Component actions ---------------------

AddComponentAction("USEITEM", "equippable", function(inst, doer, target, actions, right)
	if doer:HasTag("player") and target:HasTag("wx") then
		local action = GLOBAL.ACTIONS.GIVE
        action.priority = 3
        table.insert(actions, action)
	end
end)

AddComponentAction("USEITEM", "spellcaster", function(inst, doer, target, actions, right)
	if doer:HasTag("player") and target.prefab == "wxdiviningrodbase" and not target:HasTag("socketed") then
        table.insert(actions, GLOBAL.ACTIONS.GIVE)
	end
end)

AddComponentAction("EQUIPPED", "weapon", function(inst, doer, target, actions, right)
	if not right and doer:HasTag("player") and target:HasTag("wx") then
        table.remove(actions, actions[GLOBAL.ACTIONS.ATTACK])
	end
end)

AddComponentAction("SCENE", "shelf", function(inst, doer, actions, right)
	if doer:HasTag("player") and inst.prefab == "wxdiviningrodbase" and inst:HasTag("socketed") then
        table.insert(actions, GLOBAL.ACTIONS.TAKEITEM)
	end
end)

-- Stategraph ----------------------------

--AddStategraphState("stategraph_name", state_var)
--AddStategraphActionHandler("wilson", GLOBAL.ActionHandler(GLOBAL.ACTIONS.AUGMENT, "dolongaction"))
--AddStategraphActionHandler("wilson_client", GLOBAL.ActionHandler(GLOBAL.ACTIONS.AUGMENT, "dolongaction"))

local function SGWilsonPostInit(sg)
    local _onenter = sg.states["portal_rez"].onenter
    sg.states["portal_rez"].onenter = function(inst)
        _onenter(inst)
        if inst.prefab == "wx78" then
            inst.AnimState:SetMultColour(1, 1, 1, 1)
            inst.SoundEmitter:PlaySound("dontstarve/common/rebirth_amulet_raise")
            inst.AnimState:PushAnimation("idle_lunacy_pre", 1)
            inst.AnimState:PushAnimation("idle_lunacy_loop", 2)
            inst.AnimState:PushAnimation("idle_lunacy_loop", 3)
        end
    end

    local onexit = sg.states["portal_rez"].onexit
    sg.states["portal_rez"].onexit = function(inst)
        onexit(inst)
        if inst.prefab == "wx78" then
            inst.SoundEmitter:PlaySound("dontstarve/common/rebirth_amulet_poof")
            inst.components.talker:Say("VM MIGRATION SEQUENCE COMPLETE")
        end
    end
end

AddStategraphPostInit("wilson", SGWilsonPostInit)