local assets =
{
    Asset("ANIM", "anim/wilsonstatue.zip"),
    Asset("MINIMAP_IMAGE", "resurrect"),
}

local prefabs =
{
    "collapse_small",
    "collapse_big",
    "charcoal",
}

local function onhammeredfinished(inst, worker)
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    local fx
    if inst:HasTag("burnt") then
        if inst.components.lootdropper ~= nil then
            inst.components.lootdropper:SpawnLootPrefab("charcoal")
        end
        fx = SpawnPrefab("collapse_small")
    else
        if inst.components.lootdropper ~= nil then
            inst.components.lootdropper:DropLoot()
        end
        fx = SpawnPrefab("collapse_big")
    end
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onburnt(inst)
    inst:RemoveComponent("combat")
    inst:RemoveComponent("health")
    DefaultBurntStructureFn(inst)
end

local function onhammered(inst, worker)
    if not inst:HasTag("burnt") then
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle")
    end
end

local function onhit(inst)
    if not inst:HasTag("burnt") then
        inst.components.health:SetCurrentHealth(inst.components.health.maxhealth)
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle")
    end
end

local function onsave(inst, data)
    if inst:HasTag("burnt") or (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
        data.burnt = true
    end
end

local function onload(inst, data)
    if data ~= nil and data.burnt then
        inst.components.burnable.onburnt(inst)
    end
end

local function onbuilt(inst, data)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle", false)
end

local function keeptargetfn()
    return false
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, .3)

    inst.MiniMapEntity:SetIcon("resurrect.png")

    inst:AddTag("structure")

    inst.AnimState:SetBank("wilsonstatue")
    inst.AnimState:SetBuild("wilsonstatue")
    inst.AnimState:PlayAnimation("idle")

    MakeSnowCoveredPristine(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(onhammeredfinished)
    inst.components.workable:SetOnWorkCallback(onhammered)
    inst:ListenForEvent("onbuilt", onbuilt)

    inst:AddComponent("burnable")
    inst.components.burnable:SetFXLevel(3)
    inst.components.burnable:SetBurnTime(10)
    inst.components.burnable:AddBurnFX("fire", Vector3(0, 0, 0))
    inst.components.burnable:SetOnBurntFn(onburnt)
    MakeLargePropagator(inst)

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(TUNING.WILSON_HEALTH)
    inst.components.health.canheal = false

    inst:AddComponent("combat")
    inst.components.combat:SetKeepTargetFunction(keeptargetfn)
    inst.components.combat:SetOnHit(onhit)

    inst.OnSave = onsave
    inst.OnLoad = onload

    MakeSnowCovered(inst)
    AddHauntableDropItemOrWork(inst)

    return inst
end

return Prefab("wilsondummy", fn, assets, prefabs)
