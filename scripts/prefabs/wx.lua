local assets =
{
    Asset("SOUND", "sound/wx78.fsb"),
}

local prefabs =
{
    "sparks",
    "cracklehitfx",
    "collapse_small",
}

local brain = require "brains/wxbrain"

--------------------------
-- Life Cycle Management--
--------------------------
local function onbuilt(inst)
    inst.SoundEmitter:PlaySound("dontstarve/common/icebox_craft")
    inst.components.talker:Say("LOCAL UNIT ONLINE")
end

local function OnHammered(inst, worker)
    local inventory = inst.components.inventory
    if inventory ~= nil then
        for k, v in pairs(inventory.opencontainers) do
            k.components.container:Close(inst)
        end
    end
    inst.components.container:Close()
    inst.sg:GoToState("hammered")
end

local function OnHammeredFinished(inst, worker)
    local inventory = inst.components.inventory
    if inventory ~= nil then
        for k, v in pairs(inventory.opencontainers) do
            k.components.container:Close(inst)
        end
    end

    if inst.components.rider:IsRiding() then
        inst.components.rider:ActualDismount()
    end
    inst.components.lootdropper:DropLoot()
    inst.components.inventory:DropEverything(true)
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("metal")
    inst:Remove()
end

---------------------
-- Trade Management--
---------------------
local function ShouldAcceptItem(inst, item)
    return item.components.equippable ~= nil or item.components.inventoryitem ~= nil
end

local function OnGetItemFromPlayer(inst, giver, item)
    if item.components.equippable then
        local newslot = item.components.equippable.equipslot
        local current = inst.components.inventory:GetEquippedItem(newslot)

        if current == nil then
            inst.components.talker:Say("NEW HARDWARE INSTALLED")
        elseif current.prefab == item.prefab then
            if item.components.stackable == nil then
                inst.components.inventory:DropItem(current, true, true)
            end
        elseif newslot == EQUIPSLOTS.HEAD then
            inst.components.inventory:DropItem(current, true, true)
            local tool = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            inst.components.inventory:DropItem(tool, true, true)
            local coat = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
            inst.components.inventory:DropItem(coat, true, true)
            inst.components.inventory:DropEverything(false, true)
        else
            inst.components.inventory:DropItem(current, true, true)
        end

        inst.components.inventory:Equip(item)
        inst.components.wxnavigation.engaged = false
        inst.components.locomotor:Stop()
    end
end

local function OnRefuseItem(inst, item)
    inst.components.talker:Say("IMCOMPATIBLE HARDWARE")
end

-----------------------
-- Storage Management--
-----------------------
local containers = require("containers")
local params = containers.params
params.wx = {
    widget = {
        slotpos = {},
        animbank = "ui_tacklecontainer_3x5",
        animbuild = "ui_tacklecontainer_3x5",
        pos = Vector3(200, 80, 0)
    },
    type = "wx",
    openlimit = 2,
    itemtestfn = function(inst, item, slot)
        return not item:HasTag("_container")
    end
}

for y = 1, -3, -1 do
    for x = 0, 2 do
        table.insert(params.wx.widget.slotpos, Vector3(80 * x - 80 * 2 + 80, 80 * y - 45, 0))
    end
end

local function OnOpen(inst)
    inst.SoundEmitter:PlaySound("dontstarve/common/icebox_open")
end

local function OnClose(inst)
    inst.SoundEmitter:PlaySound("dontstarve/common/icebox_close")
end

--------------------------
-- Equipment Management --
--------------------------
local function OnWeaponBroke(inst)
    local weapon = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    if weapon == nil or weapon:IsInLimbo() then
        inst.components.combat:DropTarget()
    end
end

local function OnWeaponRanOut(inst, equipslot)
    local weapon = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    if equipslot == EQUIPSLOTS.HANDS and weapon == nil or weapon:IsInLimbo() then
        inst.components.combat:DropTarget()
    end
end

local function OnArmorBroke(inst)
    local helmet = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    local armor = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    if (helmet == nil or helmet:IsInLimbo()) and (armor == nil or armor:IsInLimbo()) then
        inst.components.combat:DropTarget()
    end
end

-----------------------
-- Combat Management --
-----------------------
local MAX_TARGET_SHARES = 5
local SHARE_TARGET_DIST = 30
local function OnAttacked(inst, data)
    local inventory = inst.components.inventory
    if inventory ~= nil then
        for k, v in pairs(inventory.opencontainers) do
            k.components.container:Close(inst)
        end
    end

    if data.attacker ~= nil then
        if data.attacker:HasTag("abigail") then
            data.attacker.components.combat:DropTarget()
        end
        if not (data.attacker:HasTag("player") or data.attacker:HasTag("wx") or
            data.attacker:HasTag("abigail") or data.attacker:HasTag("ghost")) and
            data.attacker.components.combat ~= nil then
            inst.components.combat:SuggestTarget(data.attacker)
            inst.components.combat:ShareTarget(data.target, SHARE_TARGET_DIST, function(dude) return dude:HasTag("wx") end, MAX_TARGET_SHARES)
        end
    end
end

local RETARGET_MUST_TAGS = { "_combat", "_health" }
local RETARGET_CANT_TAGS = { "player", "abigail", "playerghost", "ghost", "shadowcreature", "nightmarecreature", "wx", "INLIMBO" }
local function retargetfn(inst)
    local leader = inst.components.follower:GetLeader()
    return leader ~= nil and FindEntity(leader, TUNING.SHADOWWAXWELL_TARGET_DIST, function(guy)
        return guy ~= inst and inst.components.combat:CanTarget(guy) and
            leader.components.combat:TargetIs(guy) or
            guy.components.combat:TargetIs(inst)
    end, RETARGET_MUST_TAGS, RETARGET_CANT_TAGS) or nil
end

local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20
local function keeptargetfn(inst, target)
    return inst.components.follower.leader == nil or
        (inst.components.follower.leader ~= nil and inst.components.follower:IsNearLeader(KEEP_WORKING_DIST)) and
        inst.components.combat:CanTarget(target) and
		target.components.minigame_participator == nil
end

---------------------------
-- Hull Point Management --
---------------------------
local function OnRepaired(inst)
    inst.SoundEmitter:PlaySound("dontstarve/characters/wx78/levelup")
    inst.components.talker:Say("SHELL INTEGRITY RESTORED")
end

local function onlightingstrike(inst)
    if inst.components.health ~= nil and not (inst.components.health:IsDead() or inst.components.health:IsInvincible()) then
        if inst.components.inventory:IsInsulated() then
            inst:PushEvent("lightningdamageavoided")
        else
            local mult = TUNING.ELECTRIC_WET_DAMAGE_MULT * inst.components.moisture:GetMoisturePercent()
            local damage = TUNING.LIGHTNING_DAMAGE + mult * TUNING.LIGHTNING_DAMAGE
            inst.components.health:DoDelta(-damage, false, "lightning")
            inst.components.talker:Say("SYSTEM OVERLOAD, TAKING DAMAGE")
        end
    end
end

local function dorainsparks(inst, dt)
    if inst.components.moisture ~= nil and inst.components.moisture:GetMoisture() > 0 then
        local t = GetTime()
        local healthpercent = inst.components.health:GetPercent()
        local coefficient = (healthpercent < .8) and 0 or (5 * healthpercent - 4)

        -- Raining, no moisture-giving equipment on head, and moisture is increasing. Pro-rate damage based on waterproofness.
        if inst.components.inventory:GetEquippedMoistureRate(EQUIPSLOTS.HEAD) <= 0 and inst.components.moisture:GetRate() > 0 then
            local waterproofmult =
                (   inst.components.sheltered ~= nil and
                    inst.components.sheltered.sheltered and
                    inst.components.sheltered.waterproofness or 0
                ) +
                (   inst.components.inventory ~= nil and
                    inst.components.inventory:GetWaterproofness() or 0
                )
            if waterproofmult < 1 and t > inst.spark_time + inst.spark_time_offset + waterproofmult * 7 then
                inst.components.health:DoDelta(coefficient * TUNING.WX78_MAX_MOISTURE_DAMAGE, false, "rain")
                inst.spark_time_offset = 3 + math.random() * 2
                inst.spark_time = t
                local x, y, z = inst.Transform:GetWorldPosition()
                SpawnPrefab("sparks").Transform:SetPosition(x, y + 1 + math.random() * 1.5, z)
            end
        -- We have moisture-giving equipment on our head or it is not raining and we are just passively wet (but drying off). Do full damage.
        elseif t > inst.spark_time + inst.spark_time_offset then
            inst.components.health:DoDelta(
                inst.components.moisture:GetRate() >= 0 and
                coefficient * TUNING.WX78_MAX_MOISTURE_DAMAGE or
                coefficient * TUNING.WX78_MOISTURE_DRYING_DAMAGE,
                false, "water")
            inst.spark_time_offset = 3 + math.random() * 2
            inst.spark_time = t
            local x, y, z = inst.Transform:GetWorldPosition()
            SpawnPrefab("sparks").Transform:SetPosition(x, y + .25 + math.random() * 2, z)
        end
    end
end

local function onmoisturedelta(inst)
    if inst.components.moisture ~= nil and inst.components.moisture:GetMoisture() > 0 then
        if inst.spark_task == nil then
            inst.spark_task = inst:DoPeriodicTask(.1, dorainsparks, nil, .1)
        end
    elseif inst.spark_task ~= nil then
        inst.spark_task:Cancel()
        inst.spark_task = nil
    end
end

-----------------------
-- Riding Management --
-----------------------
local function OnDismount(inst)
    inst.components.combat:GiveUp()
    inst.components.locomotor:Stop()
end

local function OnBucked(inst)
    inst.components.combat:GiveUp()
    inst.components.locomotor:Stop()
end

-----------------------------
-- Resurrection Management --
-----------------------------
local function OnHaunt(inst, haunter)
    if haunter.prefab == "wx78" then
        local rotation = inst.Transform:GetRotation()
        haunter.Transform:SetRotation(rotation)

        if inst.task ~= nil then
            inst.task:Cancel()
            inst.task = nil
        end
        
        inst.components.talker:Say("VM MIGRATION SEQUENCE INITIATED")
        inst.components.health:SetInvincible(true)
        inst.components.inventory:DropEverything(false, false)

        --inst.Hide()
        inst.Physics:Stop()
        inst.Physics:ClearCollisionMask()
        inst.Physics:CollidesWith(COLLISION.GROUND)
        --[[
        inst.Physics:CollidesWith(COLLISION.WORLD)
        inst.Physics:CollidesWith(COLLISION.OBSTACLES)
        inst.Physics:CollidesWith(COLLISION.SMALLOBSTACLES)
        inst.Physics:CollidesWith(COLLISION.GIANTS)
        ]]
        inst.DynamicShadow:Enable(false)

        inst:ListenForEvent("rez_player", function(inst)
            if inst.components.talker ~= nil then
                inst.components.talker:ShutUp()
            end
            inst:DoTaskInTime(14 * FRAMES, function(inst)
                inst:Remove()
            end)
        end)
        return true
    else
        return false
    end
end

-------------------
-- Main Function --
-------------------
local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddDynamicShadow()
        inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()

        inst.Transform:SetFourFaced()

        inst.AnimState:SetBank("wilson")
        inst.AnimState:SetBuild("wx78")
        inst.AnimState:PlayAnimation("idle")

        inst.AnimState:Hide("ARM_carry")
        inst.AnimState:Hide("HAT")
        inst.AnimState:Hide("HAIR_HAT")
        inst.AnimState:Hide("HEAD_HAT")
        inst.AnimState:Show("HAIR_NOHAT")
        inst.AnimState:Show("HAIR")
        inst.AnimState:Show("HEAD")

        inst.DynamicShadow:SetSize(1.3, .6)

        inst.MiniMapEntity:SetIcon("wx78.png")
        inst.MiniMapEntity:SetPriority(10)
        inst.MiniMapEntity:SetCanUseCache(false)
        inst.MiniMapEntity:SetDrawOverFogOfWar(true)

        MakeCharacterPhysics(inst, 75, .5)

        inst:AddTag("character")
        inst:AddTag("notraptrigger")
        inst:AddTag("scarytoprey")
        inst:AddTag("lightningtarget")
        inst:AddTag("wx")
        inst:AddTag("NOBLOCK")
        -- For DoMoveToRezSource in player_commom_extentions.lua
        inst:AddTag("multiplayer_portal")

        inst:AddComponent("talker")
        inst.components.talker.fontsize = 35
        inst.components.talker.font = TALKINGFONT
        inst.components.talker.offset = Vector3(0,-400,0)
        inst.components.talker:StopIgnoringAll()

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst.entity:SetCanSleep(false)

        inst.spark_task = nil
        inst.spark_time = 0
        inst.spark_time_offset = 3

        inst:AddComponent("inspectable")

        inst:AddComponent("maprevealable")
        inst.components.maprevealable:AddRevealSource(inst, "wxtracker")
        inst.components.maprevealable:SetIconPriority(10)
        inst.components.maprevealable:SetIconPrefab("globalmapiconunderfog")

        inst:AddComponent("inventory")
        inst.components.inventory.maxslots = 15

        inst:AddComponent("container")
        inst.components.container:WidgetSetup("wx")
        inst.components.container.slots = inst.components.inventory.itemslots -- Holy shit I'm genius!!!
        inst.components.container.onopenfn = OnOpen
        inst.components.container.onclosefn = OnClose

        inst:AddComponent("trader")
        --inst.components.trader.acceptnontradable = true
        inst.components.trader:SetAcceptTest(ShouldAcceptItem)
        inst.components.trader.onaccept = OnGetItemFromPlayer
        inst.components.trader.onrefuse = OnRefuseItem
        inst.components.trader.deleteitemonaccept = false

        inst:AddComponent("lootdropper")

        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
        inst.components.workable:SetWorkLeft(4)
        inst.components.workable:SetOnFinishCallback(OnHammeredFinished)
        inst.components.workable:SetOnWorkCallback(OnHammered)

        inst:AddComponent("locomotor")
        inst.components.locomotor.runspeed = TUNING.WILSON_RUN_SPEED
        inst.components.locomotor:SetTriggersCreep(false)
        inst.components.locomotor.pathcaps = { ignorecreep = true }
        inst.components.locomotor:SetSlowMultiplier(.6)
        inst.components.locomotor:SetAllowPlatformHopping(true)
        if TheWorld.has_ocean then
	        inst:AddComponent("embarker")
            inst:AddComponent("drownable")
		end

        inst:AddComponent("follower")
        inst.components.follower.leader = nil
        inst.components.follower:KeepLeaderOnAttacked()
        inst.components.follower.keepdeadleader = true
        inst.components.follower.keepleaderduringminigame = true

        inst:AddComponent("entitytracker")

        inst:AddComponent("health")
        inst.components.health:SetMaxHealth(TUNING.WX78_MIN_HEALTH)
        inst.components.health.canheal = false

        inst:AddComponent("repairable")
        inst.components.repairable.repairmaterial = MATERIALS.GEARS
        inst.components.repairable.onrepaired = OnRepaired

        inst:AddComponent("combat")
        inst.components.combat.hiteffectsymbol = "torso"
        inst.components.combat:SetDefaultDamage(TUNING.UNARMED_DAMAGE)
        inst.components.combat:SetAttackPeriod(TUNING.WILSON_ATTACK_PERIOD)
        inst.components.combat:SetRange(TUNING.DEFAULT_ATTACK_RANGE)
        inst.components.combat:SetRetargetFunction(2, retargetfn)
        inst.components.combat:SetKeepTargetFunction(keeptargetfn)

        MakeLargeFreezableCharacter(inst, "torso")
        inst.components.freezable:SetResistance(4)
        inst.components.freezable:SetDefaultWearOffTime(TUNING.PLAYER_FREEZE_WEAR_OFF_TIME)

        inst:AddComponent("pinnable")
        inst:AddComponent("rider")
        inst:AddComponent("moisture")
        inst:AddComponent("sheltered")
        inst:AddComponent("stormwatcher")
        inst:AddComponent("carefulwalker")

        inst:AddComponent("playerlightningtarget")
        inst.components.playerlightningtarget:SetHitChance(TUNING.WX78_LIGHTNING_TARGET_CHANCE)
        inst.components.playerlightningtarget:SetOnStrikeFn(onlightingstrike)

        inst:AddComponent("wxtype")
        inst:AddComponent("wxstorage")

        inst:AddComponent("wxmilitary")
        inst:AddComponent("wxnavigation")
        inst:AddComponent("astarpathfinding")
        inst:AddComponent("wxagriculture")
        inst:AddComponent("wxhorticulture")
        inst:AddComponent("wxarboriculture")
        inst:AddComponent("wxapiculture")
        inst:AddComponent("wxaquaculture")
        inst:AddComponent("wxminingindustry")
        inst:AddComponent("wxpastoralism")

        inst:AddComponent("hauntable")
        inst.components.hauntable:SetHauntValue(TUNING.HAUNT_INSTANT_REZ)
        inst.components.hauntable:SetOnHauntFn(OnHaunt)

        inst:SetBrain(brain)
        inst:SetStateGraph("SGwx")

        inst:ListenForEvent("onbuilt", onbuilt)
        inst:ListenForEvent("attacked", OnAttacked)
        inst:ListenForEvent("weaponbroke", OnWeaponBroke)
        inst:ListenForEvent("itemranout", OnWeaponBroke)
        inst:ListenForEvent("armorbroke", OnArmorBroke)
        inst:ListenForEvent("moisturedelta", onmoisturedelta)
        inst:ListenForEvent("dismount", OnDismount)
        inst:ListenForEvent("bucked", OnBucked)

        return inst
end

return Prefab("wx", fn, assets, prefabs)