require "prefabutil"

local function placer(inst)
    inst.AnimState:Hide("ARM_carry")
    inst.AnimState:Hide("HAT")
    inst.AnimState:Hide("HAIR_HAT")
    inst.AnimState:Show("HAIR_NOHAT")
    inst.AnimState:Show("HAIR")
    inst.AnimState:Show("HEAD")
    inst.AnimState:Hide("HEAD_HAT")
end

return MakePlacer("scripts/prefabs/wx_placer", "wilson", "wx78", "idle", nil, nil, nil, nil, TheCamera:GetHeadingTarget(), "four", placer)