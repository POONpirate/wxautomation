require "behaviours/standstill"
require "behaviours/faceentity"
require "behaviours/follow"
require "behaviours/chaseandattack"
require "behaviours/runaway"
require "behaviours/doaction"

local WXBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

local MIN_FOLLOW_DIST = 0
local TARGET_FOLLOW_DIST = 6
local MAX_FOLLOW_DIST = 8

local START_FACE_DIST = 6
local KEEP_FACE_DIST = 8

local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20

local RUN_AWAY_DIST = 5
local STOP_RUN_AWAY_DIST = 8

local AVOID_EXPLOSIVE_DIST = 5
local function ShouldAvoidExplosive(target)
    return target.components.explosive == nil
        or target.components.burnable == nil
        or target.components.burnable:IsBurning()
end

local function GetFaceTargetFn(inst)
    local target = FindClosestPlayerToInst(inst, START_FACE_DIST, true)
    return target ~= nil and not target:HasTag("notarget") and target or nil
end

local function KeepFaceTargetFn(inst, target)
    return not target:HasTag("notarget") and inst:IsNear(target, KEEP_FACE_DIST)
end

local function ShouldRunAway(target, inst)
    if inst.components.wxtype ~= nil and inst.components.wxtype:IsApi() and target:HasTag("bee") then
        return false
    else
        return ((not (target.components.health ~= nil and target.components.health:IsDead())
            and (target.components.combat ~= nil and target.components.combat:HasTarget()))
            or (TheWorld.state.isspring and target:HasTag("bee")))
            and not target:IsAsleep()
    end
end

----------------------
-- OnStart Function --
----------------------

function WXBrain:OnStart()
    local root = PriorityNode(
    {
        -- Evade Explosives
        RunAway(self.inst, { fn = ShouldAvoidExplosive, tags = { "explosive" }, notags = { "INLIMBO" } }, AVOID_EXPLOSIVE_DIST, AVOID_EXPLOSIVE_DIST),
        -- Evade hostile
        IfNode(
            function() return not self.inst.components.wxtype:IsMili() end, "Evade",
            RunAway(self.inst, { fn = ShouldRunAway, oneoftags = { "monster", "hostile", "mosquito", "bee" },
                notags = { "player", "shadowcreature", "nightmarecreature", "INLIMBO" } }, RUN_AWAY_DIST, STOP_RUN_AWAY_DIST)
        ),

        -- Is Mili
        IfNode(
            function() return self.inst.components.wxtype:IsMili() end, "Respond",
            SelectorNode({
                -- Evade hostile
                IfNode(
                    function()
                        local helmet = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
                        local armor = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
                        local weapon = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
                        return weapon == nil or (helmet == nil and armor == nil)
                            or self.inst.components.health:GetPercent() <= .2
                    end, "Evade",
                    RunAway(self.inst, { fn = ShouldRunAway, oneoftags = { "monster", "hostile", "mosquito", "killer" },
                        notags = { "player", "shadowcreature", "nightmarecreature", "INLIMBO" } }, RUN_AWAY_DIST, STOP_RUN_AWAY_DIST)
                ),
                -- Self repair
                IfNode(function() return self.inst.components.health:GetPercent() <= .2 end, "Self Repair",
                    DoAction(self.inst, function() return self.inst.components.wxmilitary:SelfRepair() end)
                ),
                -- Pick up equipments
                IfNode(
                    function() return true end, "Pick Up",
                    DoAction(self.inst, function() return self.inst.components.wxmilitary:FindEntityToPickUpAction() end)
                ),
                -- Equip
                IfNode(
                    function() return true end, "Equip",
                    DoAction(self.inst, function() return self.inst.components.wxmilitary:FindEquipmentToEquipAction() end)
                ),
                -- Engage hostile
                IfNode(
                    function()
                        if self.inst.components.combat:HasTarget() and self.inst.components.combat.target.prefab == "wilsondummy" then
                            self.inst.components.combat:DropTarget()
                            return
                        end
                        local helmet = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
                        local armor = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
                        local weapon = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
                        return self.inst.components.health:GetPercent() > .2 and
                            (weapon ~= nil and weapon.components.weapon ~= nil and weapon.components.tool == nil and
                            FunctionOrValue(weapon.components.weapon:GetDamage(self.inst, self.inst)) >= TUNING.NIGHTSTICK_DAMAGE) and
                            ((helmet ~= nil and helmet.components.armor ~= nil) or (armor ~= nil and armor.components.armor ~= nil))
                    end, "Attack",
                    ChaseAndAttack(self.inst, 20, KEEP_WORKING_DIST)
                ),
            })
        ),

        -------------------
        -- Unmanned Mode --
        -------------------
        -- Has sentryward or no leader
        WhileNode(function()
            local leader = self.inst.components.follower.leader
            local sentryward = self.inst.components.entitytracker:GetEntity("sentryward") or
                FindEntity(self.inst, KEEP_WORKING_DIST * 2, function(ent) return ent.prefab == "sentryward" end)
            if leader == nil and self.inst.components.entitytracker:GetEntity("sentryward") == nil and sentryward ~= nil then
                self.inst.components.entitytracker:TrackEntity("sentryward", sentryward)
            end
            return leader == nil
        end, "Unmanned",
            -- Select Operation According To Type
            SelectorNode({
                -- Is Convey
                IfNode(
                    function() return self.inst.components.wxtype:IsConv() end, "Search Orders",
                    SelectorNode({
                        -- Picks up backpack
                        IfNode(
                            function() return self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY) == nil end, "Pick Up",
                            DoAction(self.inst, function() return self.inst.components.wxnavigation:FindEntityToPickUpAction() end)
                        ),
                        -- Capacity empty, goes to AP
                        IfNode(
                            function()
                                local backpack = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
                                return FindEntity(self.inst, SEE_WORK_DIST, function(ent) return ent.prefab == "sentryward" end) == nil and
                                    next(self.inst.components.inventory.itemslots) == nil and
                                    (backpack == nil or backpack.components.container:IsEmpty())
                            end, "To AP",
                            DoAction(self.inst, function() return self.inst.components.wxnavigation:AutoNavigate(true) end)
                        ),
                        -- Loads cargo
                        IfNode(
                            function()
                                local backpack = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
                                return not self.inst.components.inventory:IsFull() or
                                    (backpack ~= nil and not backpack.components.container:IsFull())
                            end, "Load",
                            DoAction(self.inst, function() return self.inst.components.wxnavigation:LoadCargo() end)
                        ),
                        -- Cannot find more or load more, goes to base
                        IfNode(
                            function()
                                local backpack = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
                                return FindEntity(self.inst, SEE_WORK_DIST, function(ent) return ent.prefab == "wxdiviningrodbase" end) == nil and
                                    next(self.inst.components.inventory.itemslots) ~= nil or
                                    (backpack ~= nil and not backpack.components.container:IsEmpty())
                            end, "To Base",
                            DoAction(self.inst, function() return self.inst.components.wxnavigation:AutoNavigate(false) end)
                        ),
                        -- Stores cargo
                        IfNode(
                            function()
                                local backpack = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
                                return next(self.inst.components.inventory.itemslots) ~= nil or
                                    (backpack ~= nil and not backpack.components.container:IsEmpty())
                            end, "Unload",
                            DoAction(self.inst, function() return self.inst.components.wxnavigation:UnloadCargo() end)
                        ),
                    })
                ),
                -- Is Agri
                IfNode(
                    function() return self.inst.components.wxtype:IsAgri() end, "Farm",
                    SelectorNode({
                        -- Picks up farming tools
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Pick Up",
                            DoAction(self.inst, function() return self.inst.components.wxagriculture:FindEntityToPickUpAction() end)
                        ),
                        -- Harvests crops
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Harvest",
                            DoAction(self.inst, function() return self.inst.components.wxagriculture:HarvestCrops() end)
                        ),
                        -- Hammers crops
                        IfNode(
                            function() return self.inst.components.wxtype:CanDoAction(ACTIONS.HAMMER, true) end, "Harvest",
                            DoAction(self.inst, function() return self.inst.components.wxagriculture:HammerCrops() end)
                        ),
                        -- Stores veggies
                        IfNode(
                            function()
                                local seedpouch = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
                                return self.inst.components.inventory:FindItem(function(item)
                                    return (item.components.edible ~= nil and item.components.perishable ~= nil and
                                        (item.components.edible.foodtype ~= FOODTYPE.SEEDS or (seedpouch ~= nil and item.prefab == "seeds"))) or
                                        item.prefab == "messagebottleempty"
                                end)
                            end, "Store",
                            DoAction(self.inst, function() return self.inst.components.wxagriculture:StoreVeggies() end)
                        ),
                        -- Digs debris
                        IfNode(
                            function() return self.inst.components.wxtype:CanDoAction(ACTIONS.DIG, true) end, "Dig Debris",
                            DoAction(self.inst, function() return self.inst.components.wxagriculture:DIG() end)
                        ),
                        -- Tills soil
                        IfNode(
                            function() return self.inst.components.wxtype:CanDoAction(ACTIONS.TILL, true) end, "Till Ground",
                            DoAction(self.inst, function() return self.inst.components.wxagriculture:Till() end)
                        ),
                        -- Plants seeds
                        IfNode(
                            function() return true end, "Plant Seeds",
                            DoAction(self.inst, function() return self.inst.components.wxagriculture:PlantSeeds() end)
                        ),
                        -- MOISTURE LOW
                        IfNode(
                            function() return self.inst.components.wxtype:CanDoAction(ACTIONS.POUR_WATER_GROUNDTILE, true)
                            end, "Water Ground",
                            DoAction(self.inst, function() return self.inst.components.wxagriculture:Water() end)
                        ),
                        -- NUTRIENTS LOW
                        IfNode(
                            function()
                                return self.inst.components.inventory:FindItem(function(item)
                                    return item.components.fertilizer ~= nil
                                end)
                            end, "Fertilize Ground",
                            DoAction(self.inst, function() return self.inst.components.wxagriculture:Fertilize() end)
                        ),
                    })
                ),
                -- Is Horti
                IfNode(
                    function() return self.inst.components.wxtype:IsHorti() end, "Gather",
                    SelectorNode({
                        -- Picks up materials
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Pick Up",
                            DoAction(self.inst, function() return self.inst.components.wxhorticulture:FindEntityToPickUpAction() end)
                        ),
                        -- Harvests fruits
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Harvest",
                            DoAction(self.inst, function() return self.inst.components.wxhorticulture:HarvestFruits() end)
                        ),
                        -- Stores veggies
                        IfNode(
                            function()
                                return self.inst.components.inventory:FindItem(function(item)
                                    return item.components.fertilizer == nil
                                end)
                            end, "Store",
                            DoAction(self.inst, function() return self.inst.components.wxhorticulture:StoreVeggies() end)
                        ),
                        -- Fertilize withered plants
                        IfNode(
                            function()
                                return self.inst.components.inventory:FindItem(function(item)
                                    return item.components.fertilizer ~= nil
                                end)
                            end, "Fertilize",
                            DoAction(self.inst, function() return self.inst.components.wxhorticulture:Fertilize() end)
                        ),
                    })
                ),
                -- Is Arbori
                IfNode(
                    function() return self.inst.components.wxtype:IsArbori() end, "Prune",
                    SelectorNode({
                        -- Picks up materials
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Pich Up",
                            DoAction(self.inst, function() return self.inst.components.wxarboriculture:FindEntityToPickUpAction() end)
                        ),
                        -- Digs stumps
                        IfNode(
                            function()
                                return self.inst.components.wxtype:CanDoAction(ACTIONS.DIG, true) and not self.inst.components.inventory:IsFull()
                            end, "Dig",
                            DoAction(self.inst, function() return self.inst.components.wxarboriculture:DIG() end)
                        ),
                        -- Plants seeds
                        IfNode(
                            function()
                                return self.inst.components.inventory:FindItem(function(item)
                                    return item.components.deployable ~= nil and item.components.deployable.mode == DEPLOYMODE.PLANT
                                end)
                            end, "Plant Seeds",
                            DoAction(self.inst, function() return self.inst.components.wxarboriculture:PlantSeeds() end)
                        ),
                        -- Stores wood
                        IfNode(
                            function() return #self.inst.components.inventory.itemslots > 0 end, "Store",
                            DoAction(self.inst, function() return self.inst.components.wxarboriculture:StoreWood() end)
                        ),
                        -- Chops trees
                        IfNode(
                            function()
                                return self.inst.components.wxtype:CanDoAction(ACTIONS.CHOP, true) and not self.inst.components.inventory:IsFull()
                            end, "Chop",
                            DoAction(self.inst, function() return self.inst.components.wxarboriculture:Chop() end)
                        ),
                    })
                ),
                -- Is Api
                IfNode(
                    function() return self.inst.components.wxtype:IsApi() end, "Raise Bees",
                    SelectorNode({
                        -- Harvests Honey
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Harvest",
                            DoAction(self.inst, function() return self.inst.components.wxapiculture:HarvestBeeBox() end)
                        ),
                        -- Stores Honey
                        IfNode(
                            function() return #self.inst.components.inventory.itemslots > 0 end, "Store",
                            DoAction(self.inst, function() return self.inst.components.wxapiculture:StoreHoney() end)
                        ),
                    })
                ),
                -- Is Aqua
                IfNode(
                    function() return self.inst.components.wxtype:IsAqua() end, "Fish",
                    SelectorNode({
                        -- Picks up fish
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Pick Up",
                            DoAction(self.inst, function() return self.inst.components.wxaquaculture:FindEntityToPickUpAction() end)
                        ),
                        -- Murders fish
                        IfNode(
                            function()
                                return self.inst.components.inventory:FindItem(function(item) return item.components.murderable ~= nil end)
                            end, "Murder",
                            DoAction(self.inst, function() return self.inst.components.wxaquaculture:MurderFish() end)
                        ),
                        -- Stores fish
                        IfNode(
                            function() return #self.inst.components.inventory.itemslots > 0 end, "Store",
                            DoAction(self.inst, function() return self.inst.components.wxaquaculture:StoreFish() end)
                        ),
                        -- Fishes in a pond
                        IfNode(
                            function()
                                return self.inst.components.wxtype:CanDoAction(ACTIONS.FISH, true) and not self.inst.components.inventory:IsFull()
                            end, "Fish",
                            DoAction(self.inst, function() return self.inst.components.wxaquaculture:PondFish() end)
                        ),
                    })
                ),
                -- Is MiningInd
                IfNode(
                    function() return self.inst.components.wxtype:IsMiningInd() end, "Extract",
                    SelectorNode({
                        -- Picks up materials
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Pich Up",
                            DoAction(self.inst, function() return self.inst.components.wxminingindustry:FindEntityToPickUpAction() end)
                        ),
                        -- Stores minerals
                        IfNode(
                            function() return #self.inst.components.inventory.itemslots > 0 end, "Store",
                            DoAction(self.inst, function() return self.inst.components.wxminingindustry:StoreMineral() end)
                        ),
                        -- Mines rocks
                        IfNode(
                            function()
                                return self.inst.components.wxtype:CanDoAction(ACTIONS.MINE, true) and not self.inst.components.inventory:IsFull()
                            end, "Mine",
                            DoAction(self.inst, function() return self.inst.components.wxminingindustry:Mine() end)
                        ),
                    })
                ),
                -- Is Pastoral
                IfNode(
                    function() return self.inst.components.wxtype:IsPast() end, "Raise Herds",
                    SelectorNode({
                        -- Picks up materials
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Pick Up",
                            DoAction(self.inst, function() return self.inst.components.wxpastoralism:FindEntityToPickUpAction() end)
                        ),
                        -- Feed herds
                        IfNode(
                            function()
                                return self.inst.components.inventory:FindItem(function(item)
                                    return item.components.edible ~= nil and not self.inst.components.rider:IsRiding()
                                end)
                            end, "Feed",
                            DoAction(self.inst, function() return self.inst.components.wxpastoralism:Feed() end)
                        ),
                        -- Stores Fuel
                        IfNode(
                            function() return #self.inst.components.inventory.itemslots > 0 and
                                not self.inst.components.rider:IsRiding()
                            end, "Store",
                            DoAction(self.inst, function() return self.inst.components.wxpastoralism:StoreFuel() end)
                        ),
                        -- Mount
                        IfNode(
                            function() return not self.inst.components.rider:IsRiding() end, "Mount",
                            DoAction(self.inst, function() return self.inst.components.wxpastoralism:Mount() end)
                        ),
                        -- Dismount
                        IfNode(
                            function() return self.inst.components.rider:IsRiding() end, "Dismount",
                            DoAction(self.inst, function() return self.inst.components.wxpastoralism:Dismount() end)
                        ),
                        -- Train racer
                        IfNode(
                            function() return self.inst.components.rider:IsRiding() end, "Work Out",
                            DoAction(self.inst, function() return self.inst.components.wxpastoralism:WorkOut() end)
                        ),
                        -- Train soldier
                        IfNode(
                            function()
                                local dummy = FindEntity(self.inst, SEE_WORK_DIST, function(ent)
                                    return ent.prefab == "wilsondummy" and ent.components.combat ~= nil
                                end)
                                local cane = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
                                self.inst.components.combat:SuggestTarget(dummy)
                                return dummy ~= nil and (cane == nil or cane.prefab ~= "cane") and
                                    self.inst.components.rider:IsRiding()
                            end, "Work Out",
                            ChaseAndAttack(self.inst, 10, KEEP_WORKING_DIST, 1)
                        ),
                        -- Brush
                        IfNode(
                            function()
                                return self.inst.components.wxtype:CanDoAction(ACTIONS.BRUSH, true) and
                                    not self.inst.components.rider:IsRiding()
                            end, "Brush",
                            DoAction(self.inst, function() return self.inst.components.wxpastoralism:Brush() end)
                        ),
                    })
                ),
                -- Leashed by the sentryward
                IfNode(
                    function()
                        if self.inst.components.wxtype:IsConv() then
                            if self.inst.task ~= nil then
                                self.inst.task:Cancel()
                                self.inst.task = nil
                            end
                            return
                        end
                        return true
                    end, "Regress",
                    LeashAndAvoid(self.inst, nil, RUN_AWAY_DIST, function()
                        local sentryward = self.inst.components.entitytracker:GetEntity("sentryward")
                        if sentryward == nil and self.inst.task == nil then
                            self.inst.task = self.inst:DoPeriodicTask(10, function()
                                self.inst.components.talker:Say("WARNING: NO OCUVIGIL DETECTED")
                            end, 0)
                        elseif sentryward ~= nil then
                            if self.inst:IsNear(sentryward, KEEP_WORKING_DIST) and self.inst.task ~= nil then
                                self.inst.task:Cancel()
                                self.inst.task = nil
                            end
                            if self.inst:IsNear(sentryward, TARGET_FOLLOW_DIST) then
                                self.inst.components.locomotor:Stop()
                            end
                        end
                        return sentryward ~= nil and sentryward:GetPosition() or nil
                    end, KEEP_WORKING_DIST, TARGET_FOLLOW_DIST, true)
                ),
            }, .25)
        ),

        -----------------
        -- Follow Mode --
        -----------------
        -- Has player leader
        WhileNode(function()
            local leader = self.inst.components.follower.leader
            if leader ~= nil and self.inst.components.entitytracker:GetEntity("sentryward") ~= nil then
                self.inst.components.entitytracker:ForgetEntity("sentryward")
            end
            return leader ~= nil and leader:HasTag("player")
        end, "Leader In Range",
            -- Select Operation According To Type
            SelectorNode({
                -- Is Horti
                IfNode(
                    function() return self.inst.components.wxtype:IsHorti() end, "Gather",
                    SelectorNode({
                        -- Picks up materials
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Pick Up",
                            DoAction(self.inst, function() return self.inst.components.wxhorticulture:FindEntityToPickUpAction() end)
                        ),
                        -- Harvests fruits
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Harvest",
                            DoAction(self.inst, function() return self.inst.components.wxhorticulture:HarvestFruits() end)
                        ),
                    })
                ),
                -- Is Arbori
                IfNode(
                    function() return self.inst.components.wxtype:IsArbori() end, "Prune",
                    SelectorNode({
                        -- Picks up materials
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Pich Up",
                            DoAction(self.inst, function() return self.inst.components.wxarboriculture:FindEntityToPickUpAction() end)
                        ),
                        -- Digs stumps
                        IfNode(
                            function()
                                return self.inst.components.wxtype:CanDoAction(ACTIONS.DIG, true) and not self.inst.components.inventory:IsFull()
                            end, "Dig",
                            DoAction(self.inst, function() return self.inst.components.wxarboriculture:DIG() end)
                        ),
                        -- Chops trees
                        IfNode(
                            function()
                                return self.inst.components.wxtype:CanDoAction(ACTIONS.CHOP, true) and not self.inst.components.inventory:IsFull()
                            end, "Chop",
                            DoAction(self.inst, function() return self.inst.components.wxarboriculture:Chop() end)
                        ),
                    })
                ),
                -- Is Aqua
                IfNode(
                    function() return self.inst.components.wxtype:IsAqua() end, "Fish",
                    SelectorNode({
                        -- Picks up fish
                        IfNode(
                            function()
                                return not self.inst.components.inventory:IsFull() and
                                    not self.inst.sg:HasStateTag("fishing")
                            end, "Pick Up",
                            DoAction(self.inst, function() return self.inst.components.wxaquaculture:FindEntityToPickUpAction() end)
                        ),
                        -- Murders fish
                        IfNode(
                            function()
                                return self.inst.components.inventory:FindItem(function(item) return item.components.murderable ~= nil end) and
                                    not self.inst.sg:HasStateTag("fishing")
                            end, "Murder",
                            DoAction(self.inst, function() return self.inst.components.wxaquaculture:MurderFish() end)
                        ),
                        -- Stores fish
                        IfNode(
                            function()
                                return self.inst:GetCurrentPlatform() ~= nil and
                                    #self.inst.components.inventory.itemslots > 0 and
                                    not self.inst.sg:HasStateTag("fishing")
                            end, "Store",
                            DoAction(self.inst, function() return self.inst.components.wxaquaculture:StoreFish() end)
                        ),
                        -- Fishes in a pond
                        IfNode(
                            function()
                                return self.inst:GetCurrentPlatform() == nil and
                                    self.inst.components.wxtype:CanDoAction(ACTIONS.FISH, true) and
                                    not self.inst.components.inventory:IsFull()
                            end, "Fish",
                            DoAction(self.inst, function() return self.inst.components.wxaquaculture:PondFish() end)
                        ),
                        -- Fishes in the ocean
                        IfNode(
                            function()
                                return self.inst:GetCurrentPlatform() ~= nil and
                                    self.inst.components.wxtype:CanDoAction(ACTIONS.OCEAN_FISHING_CAST, true)
                            end, "Fish",
                            DoAction(self.inst, function() return self.inst.components.wxaquaculture:OceanFish() end)
                        ),
                    })
                ),
                -- Is MiningInd
                IfNode(
                    function() return self.inst.components.wxtype:IsMiningInd() end, "Extract",
                    SelectorNode({
                        -- Picks up materials
                        IfNode(
                            function() return not self.inst.components.inventory:IsFull() end, "Pich Up",
                            DoAction(self.inst, function() return self.inst.components.wxminingindustry:FindEntityToPickUpAction() end)
                        ),
                        -- Mines rocks
                        IfNode(
                            function()
                                return self.inst.components.wxtype:CanDoAction(ACTIONS.MINE, true) and not self.inst.components.inventory:IsFull()
                            end, "Mine",
                            DoAction(self.inst, function() return self.inst.components.wxminingindustry:Mine() end)
                        ),
                    })
                ),
                -- Is Conv
                IfNode(
                    function()
                        return self.inst.components.wxtype:IsConv() and
                            self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY) == nil
                    end, "Pick Up",
                    DoAction(self.inst, function() return self.inst.components.wxnavigation:FindEntityToPickUpAction() end)
                ),
            }, .25)
        ),

        -- Close chest
        IfNode(
            function() return next(self.inst.components.inventory.opencontainers) ~= nil end, "Close Chest",
            DoAction(self.inst, function()
                -- The index of opencontainers is inst type.
                local container = next(self.inst.components.inventory.opencontainers)
                return BufferedAction(self.inst, container, ACTIONS.RUMMAGE)
            end)
        ),

        -- Follow and face player leader
        WhileNode(function() return self.inst.components.follower.leader ~= nil end, "Has Leader",
            SelectorNode({
                -- Jump into wormhole
                IfNode(
                    function()
                        return not self.inst.sg:HasStateTag("busy") and
                            not self.inst:IsNear(self.inst.components.follower.leader, KEEP_WORKING_DIST)
                    end, "Jump Into",
                    DoAction(self.inst, function()
                        local wormhole = FindEntity(self.inst, SEE_WORK_DIST, function(ent) return ent.prefab == "wormhole" end)
                        return wormhole ~= nil and BufferedAction(self.inst, wormhole, ACTIONS.JUMPIN) or nil
                    end)
                ),
                -- Follow player leader
                Follow(self.inst, function() return self.inst.components.follower.leader end, MIN_FOLLOW_DIST, TARGET_FOLLOW_DIST, MAX_FOLLOW_DIST),
                -- Face player leader
                WhileNode(function() return not self.inst.sg:HasStateTag("fishing") end, "Face Leader",
                    FaceEntity(self.inst, GetFaceTargetFn, KeepFaceTargetFn)
                ),
            }, .25)
        ),
    }, .25)

    self.bt = BT(self.inst, root)
end

return WXBrain