local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20

local WXMilitary = Class(function(self, inst)
    self.inst = inst
end)

local lootList =
{
    "monstermeat",
    "spidergland",
    "silk",
    "houndstooth",
    "tentaclespots",
    "stinger",
    "slurper_pelt",
    "beardhair",
}

function WXMilitary:SelfRepair()
    return BufferedAction(self.inst, nil, ACTIONS.SELFREPAIR)
end

local function FindItemToTakeAction(inst)
    local sentryward = inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    local target = FindEntity(sentryward, SEE_WORK_DIST, function(chest)
        return chest.prefab == "treasurechest" and chest.components.container ~= nil and
            chest.components.container:FindItem(function(item)
                return (item.components.armor ~= nil and
                    item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HEAD and
                    inst.components.inventory:FindItem(function(item)
                        return item.components.armor ~= nil and
                        item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HEAD
                    end) == nil) or
                    (item.components.armor ~= nil and
                    item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.BODY and
                    inst.components.inventory:FindItem(function(item)
                        return item.components.armor ~= nil and
                        item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.BODY
                    end) == nil) or
                    (item.components.weapon ~= nil and item.components.tool == nil and
                    FunctionOrValue(item.components.weapon:GetDamage(inst, inst)) >= TUNING.NIGHTSTICK_DAMAGE and
                    item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HANDS and
                    inst.components.inventory:FindItem(function(item)
                        return item.components.weapon ~= nil and
                        item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HANDS
                    end) == nil)
            end)
    end)
    local item = nil
    if target ~= nil then
        item = target.components.container:FindItem(function(item)
            return (item.components.armor ~= nil and
                item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HEAD and
                inst.components.inventory:FindItem(function(item)
                    return item.components.armor ~= nil and
                    item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HEAD
                end) == nil) or
                (item.components.armor ~= nil and
                item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.BODY and
                inst.components.inventory:FindItem(function(item)
                    return item.components.armor ~= nil and
                    item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.BODY
                end) == nil) or
                (item.components.weapon ~= nil and item.components.tool == nil and
                FunctionOrValue(item.components.weapon:GetDamage(inst, inst)) >= TUNING.NIGHTSTICK_DAMAGE and
                item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HANDS and
                inst.components.inventory:FindItem(function(item)
                    return item.components.weapon ~= nil and
                    item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HANDS
                end) == nil)
        end)
    end
    return (target ~= nil and item ~= nil) and BufferedAction(inst, target, ACTIONS.LOAD, item) or nil
end

local TOPICKUP_CANT_TAGS = { "fire", "smolder", "INLIMBO", "NOCLICK", "event_trigger", "catchable", "irreplaceable", "heavy", "outofreach" }
function WXMilitary:FindEntityToPickUpAction()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward") or self.inst

    local helmet = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    local armor = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    local weapon = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)

    local target = FindEntity(leader, SEE_WORK_DIST, function(item)
        return item ~= nil and
        item:IsValid() and
        not item:IsInLimbo() and
        item.entity:IsVisible() and
        item:IsOnValidGround() and
        item.components.inventoryitem ~= nil and
        item.components.inventoryitem.canbepickedup and
        --[[not (item.components.burnable ~= nil and
            (item.components.burnable:IsBurning() or
            item.components.burnable:IsSmoldering())) and]]
        -- Target item is armor or weapon
        ((item.components.armor ~= nil and
        item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HEAD and
        self.inst.components.inventory:FindItem(function(item)
            return item.components.armor ~= nil and
            item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HEAD
        end) == nil) or
        (item.components.armor ~= nil and
        item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.BODY and
        self.inst.components.inventory:FindItem(function(item)
            return item.components.armor ~= nil and
            item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.BODY
        end) == nil) or
        (item.components.weapon ~= nil and item.components.tool == nil and
        FunctionOrValue(item.components.weapon:GetDamage(self.inst, self.inst)) >= TUNING.NIGHTSTICK_DAMAGE and
        item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HANDS and
        self.inst.components.inventory:FindItem(function(item)
            return item.components.weapon ~= nil and item.components.tool == nil and
                FunctionOrValue(item.components.weapon:GetDamage(self.inst, self.inst)) >= TUNING.NIGHTSTICK_DAMAGE and
                item.components.equippable ~= nil and item.components.equippable.equipslot == EQUIPSLOTS.HANDS
        end) == nil) or
        table.contains(lootList, item.prefab))
    end, nil, TOPICKUP_CANT_TAGS)

    return target ~= nil and BufferedAction(self.inst, target, ACTIONS.PICKUP) or FindItemToTakeAction(self.inst) or nil
end

local function FindEquipment(inst, emptyslottype)
    local equipment = nil
    for k, v in pairs(inst.components.inventory.itemslots) do
        if v.components.equippable ~= nil and v.components.equippable.equipslot == emptyslottype and
            (v.components.armor ~= nil or (v.components.weapon ~= nil and v.components.tool == nil and
            FunctionOrValue(v.components.weapon:GetDamage(inst, inst)) >= TUNING.NIGHTSTICK_DAMAGE)) then
            equipment = v
        elseif not table.contains(lootList, v.prefab) then
            inst.components.inventory:DropItem(v, true, true)
        end
    end
    return equipment
end

function WXMilitary:FindEquipmentToEquipAction()
    local helmet = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    local armor = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    local weapon = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    if helmet == nil or helmet.components.armor == nil then
        self.inst.components.inventory:DropItem(helmet, true, true)
        self.inst.components.inventory:Equip(FindEquipment(self.inst, EQUIPSLOTS.HEAD))
    end
    if armor == nil or armor.components.armor == nil then
        self.inst.components.inventory:DropItem(armor, true, true)
        self.inst.components.inventory:Equip(FindEquipment(self.inst, EQUIPSLOTS.BODY))
    end
    if weapon == nil or weapon.components.weapon == nil or weapon.components.tool ~= nil or
        FunctionOrValue(weapon.components.weapon:GetDamage(self.inst, self.inst)) < TUNING.NIGHTSTICK_DAMAGE then
        self.inst.components.inventory:DropItem(weapon, true, true)
        self.inst.components.inventory:Equip(FindEquipment(self.inst, EQUIPSLOTS.HANDS))
    end
end

return WXMilitary