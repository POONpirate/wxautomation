local WXStorage = Class(function(self, inst)
    self.inst = inst
end)

function WXStorage:ShouldHarvest(plant)
    local product = plant.components.pickable.product
    return self.inst.components.inventory:FindItem(function(item)
        return item.prefab == product and (item.components.stackable == nil or item.components.stackable:IsFull())
    end) == nil
end

function WXStorage:ShouldPickUp(itemtobepickedup)
    return self.inst.components.follower.leader ~= nil or
        self.inst.components.inventory:FindItem(function(item)
            return item.prefab == itemtobepickedup.prefab and (item.components.stackable == nil or item.components.stackable:IsFull())
        end) == nil
end

return WXStorage