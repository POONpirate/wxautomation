local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20

local WXAquaculture = Class(function(self, inst)
    self.inst = inst
end)

local function FindBetterTackle(rod, tacklecontainer, fish)
    local fish_lure_prefs = fish ~= nil and fish.fish_def.lures or nil

    local weather = TheWorld.state.israining and "raining"
        or TheWorld.state.issnowing and "snowing"
        or "default"

    local tackle = rod.components.oceanfishingrod.gettackledatafn ~= nil and rod.components.oceanfishingrod.gettackledatafn(rod) or {}
    local bobber_data = (tackle.bobber ~= nil and tackle.bobber.components.oceanfishingtackle ~= nil) and
        tackle.bobber.components.oceanfishingtackle.casting_data or TUNING.OCEANFISHING_TACKLE.BASE
    local lure_data = (tackle.lure ~= nil and tackle.lure.components.oceanfishingtackle ~= nil) and
        tackle.lure.components.oceanfishingtackle.lure_data or TUNING.OCEANFISHING_LURE.HOOK
    local mod = ((tackle.lure ~= nil and tackle.lure.components.perishable ~= nil) and tackle.lure.components.perishable:GetPercent() or 1)
        * (lure_data.timeofday ~= nil and lure_data.timeofday[TheWorld.state.phase] or 0)
        * (fish_lure_prefs == nil and 1 or lure_data.style ~= nil and fish_lure_prefs[lure_data.style] or 0)
        * (lure_data.weather ~= nil and lure_data.weather[weather] or TUNING.OCEANFISHING_LURE_WEATHER_DEFAULT[weather] or 1)

    -- bobber
    local bestbobber = nil
    tacklecontainer.components.container:ForEachItem(function(item)
        local item_data = item.components.oceanfishingtackle ~= nil and
            item.components.oceanfishingtackle.casting_data or TUNING.OCEANFISHING_TACKLE.BASE
        local bestbobber_data = bestbobber ~= nil and
            bestbobber.components.oceanfishingtackle.casting_data or TUNING.OCEANFISHING_TACKLE.BASE
        -- Only consider casting distance.
        if bobber_data.dist_max < item_data.dist_max and (bestbobber == nil or bestbobber_data.dist_max < item_data.dist_max) then
            bestbobber = item
        end
    end)

    -- lure
    local bestlure = nil

    local modedbestcharm = nil
    tacklecontainer.components.container:ForEachItem(function(item)
        local item_data = item.components.oceanfishingtackle ~= nil and
            item.components.oceanfishingtackle.lure_data or TUNING.OCEANFISHING_LURE.HOOK
        local bestlure_data = bestlure ~= nil and
            bestlure.components.oceanfishingtackle.lure_data or TUNING.OCEANFISHING_LURE.HOOK
        local itemmod = (item.components.perishable ~= nil and item.components.perishable:GetPercent() or 1)
            * (item_data.timeofday ~= nil and item_data.timeofday[TheWorld.state.phase] or 0)
            * (fish_lure_prefs == nil and 1 or item_data.style ~= nil and fish_lure_prefs[item_data.style] or 0)
            * (item_data.weather ~= nil and item_data.weather[weather] or TUNING.OCEANFISHING_LURE_WEATHER_DEFAULT[weather] or 1)
        -- Only consider charm. Academically we should use charm * area.
        if lure_data.charm * mod < item_data.charm * itemmod and
            (bestlure == nil or modedbestcharm == nil or modedbestcharm < item_data.charm * itemmod) then
            bestlure = item
            modedbestcharm = item_data.charm * itemmod
        end
    end)
    return bestbobber, bestlure, tackle.bobber, tackle.lure
end

function WXAquaculture:OceanFish()
    local leader = self.inst.components.follower.leader
    if leader == nil then
        return nil
    end

    self.inst.components.wxtype:SwapTool(ACTIONS.OCEAN_FISHING_CAST)
    local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    if tool == nil or tool.components.oceanfishingrod == nil then
        return nil
    elseif tool.components.oceanfishingrod.target ~= nil and tool.components.oceanfishingrod.target:HasTag("oceanfish") then
        if tool.components.oceanfishingrod.target:HasTag("oceachfishing_catchable") then
            return BufferedAction(self.inst, nil, ACTIONS.OCEAN_FISHING_CATCH)
        elseif not tool.components.oceanfishingrod:IsLineTensionHigh() then
            return BufferedAction(self.inst, nil, ACTIONS.OCEAN_FISHING_REEL)
        else
            -- Loose the line
            return nil
        end
    elseif self.inst.sg:HasStateTag("fishing") and not self.inst.AnimState:IsCurrentAnimation("fishing_ocean_pst") and
        (not self.inst:IsNear(leader, KEEP_WORKING_DIST) or
        (FindEntity(self.inst, SEE_WORK_DIST, function(ent)
            return ent:HasTag("oceanfish") and ent:IsValid() and
                TheWorld.Map:GetPlatformAtPoint(ent.Transform:GetWorldPosition()) == nil and
                TheWorld.Map:IsOceanAtPoint(ent.Transform:GetWorldPosition())
        end) == nil and
        (tool.components.oceanfishingrod.target == nil or not tool.components.oceanfishingrod.target:HasTag("oceanfish")))) then
        return BufferedAction(self.inst, nil, ACTIONS.OCEAN_FISHING_STOP)
    elseif not self.inst.sg:HasStateTag("fishing") and self.inst:IsNear(leader, KEEP_WORKING_DIST) then
        local fish = FindEntity(self.inst, SEE_WORK_DIST, function(ent)
            return ent:HasTag("oceanfish") and ent:IsValid() and
                TheWorld.Map:GetPlatformAtPoint(ent.Transform:GetWorldPosition()) == nil and
                TheWorld.Map:IsOceanAtPoint(ent.Transform:GetWorldPosition())
        end)

        -- Check for better tackles.
        local tacklecontainer = FindEntity(leader, SEE_WORK_DIST, function(item)
            return item.prefab == "tacklecontainer" or item.prefab == "supertacklecontainer"
        end)

        if tacklecontainer ~= nil and fish ~= nil then
            local bestbobber, bestlure, bobber, lure = FindBetterTackle(tool, tacklecontainer, fish)
            --print(bestbobber, bestlure, bobber, lure)
            if bestbobber ~= nil then
                if bobber ~= nil then
                    self.inst.components.inventory:DropItem(tool, true, false)
                    return BufferedAction(self.inst, tool, ACTIONS.LOAD, bobber)
                end
                return BufferedAction(self.inst, tacklecontainer, ACTIONS.ASSEMBLE, bestbobber)
            end
            if bestlure ~= nil then
                if lure ~= nil then
                    self.inst.components.inventory:DropItem(tool, true, false)
                    return BufferedAction(self.inst, tool, ACTIONS.LOAD, lure)
                end
                return BufferedAction(self.inst, tacklecontainer, ACTIONS.ASSEMBLE, bestlure)
            end
        end

        -- No any better tackle, cast the tackle.
        return (tool ~= nil and fish ~= nil) and BufferedAction(self.inst, fish, ACTIONS.OCEAN_FISHING_CAST, tool) or nil
    else
        -- Wait for fish
        return nil
    end
end

function WXAquaculture:PondFish()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local pond = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.components.fishable ~= nil
    end)
    self.inst.components.wxtype:SwapTool(ACTIONS.FISH)
    local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    if tool == nil or tool.components.fishingrod == nil then
        return nil
    elseif tool.components.fishingrod:IsFishing() then
        if tool.components.fishingrod:HasHookedFish() or
            tool.components.fishingrod:FishIsBiting() then
            return BufferedAction(self.inst, nil, ACTIONS.REEL, tool)
        elseif not self.inst:IsNear(leader, KEEP_WORKING_DIST) then
            tool.components.fishingrod:StopFishing()
        else
            return nil
        end
    elseif pond ~= nil and not tool.components.fishingrod:IsFishing() and self.inst:IsNear(leader, KEEP_WORKING_DIST) then
        return BufferedAction(self.inst, pond, ACTIONS.FISH, tool)
    end
end

function WXAquaculture:MurderFish()
    repeat
        local pondfish = self.inst.components.inventory:FindItem(function(item)
            return item.components.murderable ~= nil and not item:HasTag("oceanfish")
        end)
        if pondfish ~= nil then
            local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            if tool ~= nil and tool.components.fishingrod ~= nil and tool.components.fishingrod:IsFishing() then
                tool.components.fishingrod:StopFishing()
                self.inst.sg:GoToState("idle")
            end
            return BufferedAction(self.inst, nil, ACTIONS.MURDER, pondfish)
        end
    until(pondfish == nil)
end

function WXAquaculture:StoreFish()
    local leader = self.inst.components.follower.leader or self.inst

    -- Store tackle
    local tacklecontainer = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return (ent.prefab == "tacklecontainer" or ent.prefab == "supertacklecontainer") and
            ent.components.container ~= nil and not ent.components.container:IsFull()
    end)
    local tackle = self.inst.components.inventory:FindItem(function(item)
        return item.components.oceanfishingtackle ~= nil
    end)
    if tacklecontainer ~= nil and tackle ~= nil then
        return BufferedAction(self.inst, tacklecontainer, ACTIONS.STORE, tackle)
    end

    -- Store live fish
    local fishbox = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.prefab == "fish_box" and ent.components.container ~= nil and not ent.components.container:IsFull()
    end)
    local fish = self.inst.components.inventory:FindItem(function(item)
        return item:HasTag("oceanfish")
    end)
    if fishbox ~= nil and fish ~= nil then
        return BufferedAction(self.inst, fishbox, ACTIONS.STORE, fish)
    end

    local sentryward = self.inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    -- Store raw fish
    local saltbox = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
        return ent.prefab == "saltbox" and ent.components.container ~= nil and not ent.components.container:IsFull()
    end)
    local icebox = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
        return ent.prefab == "icebox" and ent.components.container ~= nil and not ent.components.container:IsFull()
    end)

    if saltbox ~= nil then
        local raw_food = self.inst.components.inventory:FindItem(function(item)
            return item.components.edible ~= nil and item.components.edible.foodtype == FOODTYPE.MEAT and
                item.components.cookable ~= nil and item:HasTag("cookable")
        end)
        if raw_food ~= nil then
            local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            if tool ~= nil and tool.components.fishingrod ~= nil and tool.components.fishingrod:IsFishing() then
                tool.components.fishingrod:StopFishing()
                self.inst.sg:GoToState("idle")
            end
            return BufferedAction(self.inst, saltbox, ACTIONS.STORE, raw_food)
        end
    elseif icebox ~= nil then
        local any_food = self.inst.components.inventory:FindItem(function(item)
            return item.components.edible ~= nil and item.components.perishable ~= nil and
                item.components.edible.foodtype ~= FOODTYPE.SEEDS
        end)
        if any_food ~= nil then
            local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            if tool ~= nil and tool.components.fishingrod ~= nil and tool.components.fishingrod:IsFishing() then
                tool.components.fishingrod:StopFishing()
                self.inst.sg:GoToState("idle")
            end
            return BufferedAction(self.inst, icebox, ACTIONS.STORE, any_food)
        end
    end
end

local function FindItemToTakeAction(inst)
    local sentryward = inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    local target = FindEntity(sentryward, SEE_WORK_DIST, function(chest)
        return chest.prefab == "treasurechest" and chest.components.container ~= nil and
            chest.components.container:FindItem(function(item)
                return (item.components.fishingrod ~= nil and not inst.components.wxtype:CanDoAction(ACTIONS.FISH, true)) --or
                    --(item.components.oceanfishingrod ~= nil and not inst.components.wxtype:CanDoAction(ACTIONS.OCEAN_FISHING_CAST, true))
            end)
    end)
    local item = nil
    if target ~= nil then
        item = target.components.container:FindItem(function(item)
            return (item.components.fishingrod ~= nil and not inst.components.wxtype:CanDoAction(ACTIONS.FISH, true)) --or
                --(item.components.oceanfishingrod ~= nil and not inst.components.wxtype:CanDoAction(ACTIONS.OCEAN_FISHING_CAST, true))
        end)
    end
    if target ~= nil and item ~= nil then
        local tool = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        if tool ~= nil and tool.components.fishingrod ~= nil and tool.components.fishingrod:IsFishing() then
            tool.components.fishingrod:StopFishing()
            inst.sg:GoToState("idle")
        end
        return BufferedAction(inst, target, ACTIONS.LOAD, item)
    end
end

local TOPICKUP_CANT_TAGS = { "fire", "smolder", "INLIMBO", "NOCLICK", "event_trigger", "catchable", "irreplaceable", "heavy", "outofreach" }
function WXAquaculture:FindEntityToPickUpAction()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward") or self.inst

    local target = FindEntity(leader, SEE_WORK_DIST, function(item)
        return item ~= nil and
        item:IsValid() and
        not item:IsInLimbo() and
        item.entity:IsVisible() and
        ((item:IsOnValidGround() and self.inst:GetCurrentPlatform() == nil) or
        (item:GetCurrentPlatform() ~= nil and self.inst:GetCurrentPlatform() ~= nil)) and
        --[[(item.components.floater == nil or
        not item.components.floater:IsFloating()) and]]
        item.components.inventoryitem ~= nil and
        item.components.inventoryitem.canbepickedup and
        --[[not (item.components.burnable ~= nil and
            (item.components.burnable:IsBurning() or
            item.components.burnable:IsSmoldering())) and]]
        -- Target item is fishing rod or ocean fishing rod
        ((item.components.fishingrod ~= nil and not self.inst.components.wxtype:CanDoAction(ACTIONS.FISH, true)) or
        (item.components.oceanfishingrod ~= nil and not self.inst.components.wxtype:CanDoAction(ACTIONS.OCEAN_FISHING_CAST, true)) or
        -- Target item is fishing tackle
        (item.components.oceanfishingtackle ~= nil and self.inst.components.wxstorage:ShouldPickUp(item)) or
        -- Target item is ocean fish, accessable fish box found
        (item:HasTag("oceanfish") and FindEntity(leader, SEE_WORK_DIST, function(ent)
            return ent.prefab == "fish_box" and ent.components.container ~= nil and not ent.components.container:IsFull()
        end) ~= nil) or
        -- Target item is fish
        item:HasTag("pondfish"))
    end, nil, TOPICKUP_CANT_TAGS)

    if target ~= nil then
        local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        if tool ~= nil and tool.components.fishingrod ~= nil and tool.components.fishingrod:IsFishing() then
            tool.components.fishingrod:StopFishing()
            self.inst.sg:GoToState("idle")
        end
        return BufferedAction(self.inst, target, ACTIONS.PICKUP)
    else
        return FindItemToTakeAction(self.inst)
    end
end

return WXAquaculture