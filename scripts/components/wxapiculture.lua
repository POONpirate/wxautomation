local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20

local WXApiculture = Class(function(self, inst)
    self.inst = inst
end)

function WXApiculture:HarvestBeeBox()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local beebox = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.prefab == "beebox" and ent.components.harvestable ~= nil and ent.components.harvestable.produce > 5
    end)
    return beebox ~= nil and BufferedAction(self.inst, beebox, ACTIONS.HARVEST) or nil
end

function WXApiculture:StoreHoney()
    local sentryward = self.inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    local icebox = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
        return ent.prefab == "icebox" and ent.components.container ~= nil and not ent.components.container:IsFull()
    end)
    local any_food = self.inst.components.inventory:FindItem(function(item)
        return item.components.edible ~= nil and item.components.perishable ~= nil
    end)
    return (icebox ~= nil and any_food ~= nil) and BufferedAction(self.inst, icebox, ACTIONS.STORE, any_food) or nil
end

return WXApiculture