local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20

local WXAgriculture = Class(function(self, inst)
    self.inst = inst
end)

--TheWorld.Map:GetTileCenterPoint(tile_x, tile_y)
--TheWorld.Map:GetTileCoordsAtPoint(x, y, z)
--TheWorld.Map:GetTile(tile_x, tile_y)
--TheWorld.Map:IsFarmableSoilAtPoint(x, y, z)

local function GetFarmTurfsCoords(inst)
    local tile_x, tile_y = TheWorld.Map:GetTileCoordsAtPoint(inst.Transform:GetWorldPosition())
    local farmturfscoordsList = {}
    -- tilerange = (SEE_WORK_DIST - 2) // 4
    for i = -2, 2 do
        for j = -2, 2 do
            if TheWorld.Map:GetTile(tile_x + i, tile_y + j) == GROUND.FARMING_SOIL then
                table.insert(farmturfscoordsList, {tile_x = tile_x + i, tile_y = tile_y + j})
            end
        end
    end
    return farmturfscoordsList
end

local function GetTillPoints(tile_x, tile_y)
    local tile = TheWorld.Map:GetTile(tile_x, tile_y)
    local x, y, z = TheWorld.Map:GetTileCenterPoint(tile_x, tile_y)
    local tillpointsList = {}
    local FIND_SOIL_MUST_TAGS = { "soil" }
    local TILLSOIL_IGNORE_TAGS = { "NOCLICK" }
    --TILLSOIL_IGNORE_TAGS = { "NOBLOCK", "player", "FX", "INLIMBO", "DECOR", "WALKABLEPLATFORM", "wx" }
    local spacing = 4/3
    for i = -1, 1 do
        for j = -1, 1 do
            if TheWorld.Map:CanTillSoilAtPoint(x + i * spacing, 0, z + j * spacing) and
                -- Why does Klei spawn a nutrients_overlay at the center of a tile?!
                not next(TheSim:FindEntities(x + i * spacing, 0, z + j * spacing, .25, FIND_SOIL_MUST_TAGS, TILLSOIL_IGNORE_TAGS)) then
                table.insert(tillpointsList, Vector3(x + i * spacing, 0, z + j * spacing))
            end
        end
    end
    return tillpointsList
end

function WXAgriculture:Till()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local farmturfscoordsList = GetFarmTurfsCoords(leader)
    if #farmturfscoordsList == 0 then
        return nil
    end

    for k, v in pairs(farmturfscoordsList) do
        local tillpointsList = GetTillPoints(v.tile_x, v.tile_y)
        if #tillpointsList > 0 then
            local pt = tillpointsList[next(tillpointsList)]
            self.inst.components.wxtype:SwapTool(ACTIONS.TILL)
            local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            return tool ~= nil and BufferedAction(self.inst, nil, ACTIONS.TILL, tool, pt) or nil
        end
    end
end

function WXAgriculture:DIG()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local farm_soil_debris = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.prefab == "farm_soil_debris" or ent:HasTag("weed")
    end)
    if farm_soil_debris == nil then
        return nil
    end

    self.inst.components.wxtype:SwapTool(ACTIONS.DIG)
    local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    return (tool ~= nil and farm_soil_debris ~= nil) and BufferedAction(self.inst, farm_soil_debris, ACTIONS.DIG, tool) or nil
end

function WXAgriculture:PlantSeeds()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local farm_soil = FindEntity(leader, SEE_WORK_DIST, function(ent) return ent.prefab == "farm_soil" end, nil, { "NOCLICK" })
    local seedpouch = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    if seedpouch ~= nil and seedpouch.prefab ~= "seedpouch" then
        seedpouch = nil
    end
    local seed = self.inst.components.inventory:FindItem(function(item)
        -- Early stage, all seeds allowed.
        if seedpouch == nil then
            return item.components.farmplantable ~= nil
        -- Late stage, random seeds not allowed.
        else
            return item.components.farmplantable ~= nil and item.prefab ~= "seeds"
        end
    end)
    if seed == nil and seedpouch ~= nil then
        seed = seedpouch.components.container:FindItem(function(item) return item.components.farmplantable ~= nil and item.prefab ~= "seeds" end)
    end
    return (farm_soil ~= nil and seed ~= nil) and BufferedAction(self.inst, farm_soil, ACTIONS.PLANTSOIL, seed) or nil
end

function WXAgriculture:Water()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local thirsty_farm_plant = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.components.farmsoildrinker ~= nil and
            not TheWorld.components.farming_manager:IsSoilMoistAtPoint(ent.Transform:GetWorldPosition())
    end)
    if thirsty_farm_plant == nil then
        return nil
    end

    self.inst.components.wxtype:SwapTool(ACTIONS.POUR_WATER_GROUNDTILE)
    local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    if tool ~= nil and tool.components.finiteuses ~= nil and tool.components.finiteuses:GetUses() == 0 then
        local pond = FindEntity(leader, SEE_WORK_DIST, function(ent)
            return ent.components.watersource ~= nil
        end)
        local treasurechest = FindEntity(leader, SEE_WORK_DIST, function(ent)
            return ent.prefab == "treasurechest" and ent.components.container ~= nil and not ent.components.container:IsFull()
        end)
        if pond ~= nil then
            if pond.components.watersource.available then
                return BufferedAction(self.inst, pond, ACTIONS.FILL, tool)
            else
                return nil
            end
        elseif treasurechest ~= nil then
            return BufferedAction(self.inst, treasurechest, ACTIONS.STORE, tool)
        else
            self.inst.components.inventory:DropItem(tool, true, true)
            return nil
        end
    end
    return (tool ~= nil and tool.components.finiteuses ~= nil and tool.components.finiteuses:GetUses() > 0) and
        BufferedAction(self.inst, nil, ACTIONS.POUR_WATER_GROUNDTILE, tool, Vector3(thirsty_farm_plant.Transform:GetWorldPosition())) or nil
end

local UNIT_NUTRIENT_ONE = TUNING.GLOMMERFUEL_NUTRIENTS[1]
local UNIT_NUTRIENT_TWO = TUNING.GLOMMERFUEL_NUTRIENTS[2]
local UNIT_NUTRIENT_THREE = TUNING.GLOMMERFUEL_NUTRIENTS[3]
local function FindFertilizer(inst, n1, n2, n3)
    return inst.components.inventory:FindItem(function(item)
        if item.components.fertilizer ~= nil then
            if n1 <= UNIT_NUTRIENT_ONE then
                return item.components.fertilizer.nutrients[1] > 0
            elseif n2 <= UNIT_NUTRIENT_TWO then
                return item.components.fertilizer.nutrients[2] > 0
            elseif n3 <= UNIT_NUTRIENT_THREE then
                return item.components.fertilizer.nutrients[3] > 0
            end
        end
    end)
end

function WXAgriculture:Fertilize()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local hungry_farm_plant = FindEntity(leader, SEE_WORK_DIST, function(ent)
        local n1, n2, n3 = TheWorld.components.farming_manager:GetTileNutrients(TheWorld.Map:GetTileCoordsAtPoint(ent.Transform:GetWorldPosition()))
        return ent:HasTag("farm_plant") and n1 * n2 * n3 == 0
    end)
    if hungry_farm_plant == nil then
        return nil
    end

    local tile_x, tile_y = TheWorld.Map:GetTileCoordsAtPoint(hungry_farm_plant.Transform:GetWorldPosition())
    local n1, n2, n3 = TheWorld.components.farming_manager:GetTileNutrients(tile_x, tile_y)
    local fertilizer = FindFertilizer(self.inst, n1, n2, n3)
    return fertilizer ~= nil and 
        BufferedAction(self.inst, nil, ACTIONS.DEPLOY_TILEARRIVE, fertilizer, Vector3(hungry_farm_plant.Transform:GetWorldPosition())) or nil
end

function WXAgriculture:HarvestCrops()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local farm_plant = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.components.plantresearchable ~= nil and
            ent.components.pickable ~= nil and
            ent.components.pickable:CanBePicked()
    end)
    return farm_plant ~= nil and BufferedAction(self.inst, farm_plant, ACTIONS.PICK) or nil
end

function WXAgriculture:HammerCrops()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local farm_plant = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.components.waxable ~= nil and
            ent.components.perishable ~= nil and
            ent.components.equippable ~= nil and
            ent.components.workable ~= nil and
            ent.components.workable:GetWorkAction() == ACTIONS.HAMMER
    end)
    if farm_plant == nil then
        return nil
    end

    self.inst.components.wxtype:SwapTool(ACTIONS.HAMMER)
    local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    return (tool ~= nil and farm_plant ~= nil) and BufferedAction(self.inst, farm_plant, ACTIONS.HAMMER, tool) or nil
end

function WXAgriculture:StoreVeggies()
    local sentryward = self.inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    for k, bottle in pairs(self.inst.components.inventory.itemslots) do
        if bottle.prefab == "messagebottleempty" then
            -- Signed Chest
            local signedchest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and not ent.components.container:IsFull() and
                    FindEntity(ent, .5, function(sign)
                        return sign.components.drawable ~= nil and sign.components.drawable:GetImage() == bottle.prefab
                    end)
            end)
            if signedchest ~= nil then
                return BufferedAction(self.inst, signedchest, ACTIONS.STORE, bottle)
            end
            -- Unsigned Chest
            local unsignedchest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and
                    ent.components.container:Has(bottle.prefab, 1) and
                    FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
            end)
            if unsignedchest ~= nil and not unsignedchest.components.container:IsFull() then
                return BufferedAction(self.inst, unsignedchest, ACTIONS.STORE, bottle)
            end
            -- Empty Chest
            local emptychest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and ent.components.container:IsEmpty() and
                    FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
            end)
            if unsignedchest == nil and emptychest ~= nil then
                return BufferedAction(self.inst, emptychest, ACTIONS.STORE, bottle)
            end
        end
    end

    local saltbox = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
        return ent.prefab == "saltbox" and ent.components.container ~= nil and not ent.components.container:IsFull()
    end)
    local icebox = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
        return ent.prefab == "icebox" and ent.components.container ~= nil and not ent.components.container:IsFull()
    end)

    if saltbox ~= nil then
        local raw_food = self.inst.components.inventory:FindItem(function(item)
            return item.components.edible ~= nil and item.components.edible.foodtype == FOODTYPE.VEGGIE and
                item.components.cookable ~= nil and item:HasTag("cookable")
        end)
        return raw_food ~= nil and BufferedAction(self.inst, saltbox, ACTIONS.STORE, raw_food) or nil
    elseif icebox ~= nil then
        local seedpouch = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
        if seedpouch ~= nil and seedpouch.prefab ~= "seedpouch" then
            seedpouch = nil
        end
        local any_food = self.inst.components.inventory:FindItem(function(item)
            return item.components.edible ~= nil and item.components.perishable ~= nil and
                (item.components.edible.foodtype ~= FOODTYPE.SEEDS or (seedpouch ~= nil and item.prefab == "seeds"))
        end)
        return any_food ~= nil and BufferedAction(self.inst, icebox, ACTIONS.STORE, any_food) or nil
    end
end

local function FindItemToTakeAction(inst)
    local sentryward = inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    local seedpouch = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    if seedpouch ~= nil and seedpouch.prefab ~= "seedpouch" then
        seedpouch = nil
    end
    local target = FindEntity(sentryward, SEE_WORK_DIST, function(chest)
        return chest.components.container ~= nil and
            (chest.prefab == "treasurechest" and chest.components.container:FindItem(function(item)
                return
                    -- Gardenhoe
                    (item.components.farmtiller ~= nil and not inst.components.wxtype:CanDoAction(ACTIONS.TILL, true)) or
                    -- Shovel
                    (item.components.tool ~= nil and
                    item.components.tool:CanDoAction(ACTIONS.DIG) and
                    not inst.components.wxtype:CanDoAction(ACTIONS.DIG, true)) or
                    -- Hammer
                    (item.components.tool ~= nil and
                    item.components.tool:CanDoAction(ACTIONS.HAMMER) and
                    not inst.components.wxtype:CanDoAction(ACTIONS.HAMMER, true)) or
                    -- Wateringcan
                    (not inst.components.wxtype:CanDoAction(ACTIONS.POUR_WATER_GROUNDTILE, true) and
                    item:HasTag("wateringcan") and
                    item.components.finiteuses ~= nil and (item.components.finiteuses:GetUses() > 0 or
                    FindEntity(sentryward, SEE_WORK_DIST, function(ent) return ent.components.watersource ~= nil end))) or
                    -- Fertilizer
                    item.components.fertilizer ~= nil and
                    ((item.components.fertilizer.nutrients[1] > 0 and not FindFertilizer(inst, 0, 999, 999)) or
                    (item.components.fertilizer.nutrients[2] > 0 and not FindFertilizer(inst, 999, 0, 999)) or
                    (item.components.fertilizer.nutrients[3] > 0 and not FindFertilizer(inst, 999, 999, 0))) or
                    -- Seeds
                    (item.components.edible ~= nil and item.components.edible.foodtype == FOODTYPE.SEEDS)
            end)) or
            (chest.prefab == "icebox" and chest.components.container:FindItem(function(item)
                return
                    -- Fertilizer
                    item.components.fertilizer ~= nil and
                    ((item.components.fertilizer.nutrients[1] > 0 and not FindFertilizer(inst, 0, 999, 999)) or
                    (item.components.fertilizer.nutrients[2] > 0 and not FindFertilizer(inst, 999, 0, 999)) or
                    (item.components.fertilizer.nutrients[3] > 0 and not FindFertilizer(inst, 999, 999, 0))) or
                    -- Seeds
                    (item.components.edible ~= nil and item.components.edible.foodtype == FOODTYPE.SEEDS and
                    (seedpouch == nil or item.prefab ~= "seeds"))
            end))
    end)
    local item = nil
    if target ~= nil then
        item = target.components.container:FindItem(function(item)
            if target.prefab == "treasurechest" then
                return
                    -- Gardenhoe
                    (item.components.farmtiller ~= nil and not inst.components.wxtype:CanDoAction(ACTIONS.TILL, true)) or
                    -- Shovel
                    (item.components.tool ~= nil and
                    item.components.tool:CanDoAction(ACTIONS.DIG) and
                    not inst.components.wxtype:CanDoAction(ACTIONS.DIG, true)) or
                    -- Hammer
                    (item.components.tool ~= nil and
                    item.components.tool:CanDoAction(ACTIONS.HAMMER) and
                    not inst.components.wxtype:CanDoAction(ACTIONS.HAMMER, true)) or
                    -- Wateringcan
                    (not inst.components.wxtype:CanDoAction(ACTIONS.POUR_WATER_GROUNDTILE, true) and
                    item:HasTag("wateringcan") and
                    item.components.finiteuses ~= nil and (item.components.finiteuses:GetUses() > 0 or
                    FindEntity(sentryward, SEE_WORK_DIST, function(ent) return ent.components.watersource ~= nil end))) or
                    -- Fertilizer
                    (item.components.fertilizer ~= nil and
                    ((item.components.fertilizer.nutrients[1] > 0 and not FindFertilizer(inst, 0, 999, 999)) or
                    (item.components.fertilizer.nutrients[2] > 0 and not FindFertilizer(inst, 999, 0, 999)) or
                    (item.components.fertilizer.nutrients[3] > 0 and not FindFertilizer(inst, 999, 999, 0)))) or
                    -- Seeds
                    (item.components.edible ~= nil and item.components.edible.foodtype == FOODTYPE.SEEDS and
                    (seedpouch == nil or item.prefab ~= "seeds"))
            elseif target.prefab == "icebox" then
                return
                    -- Fertilizer
                    item.components.fertilizer ~= nil and
                    ((item.components.fertilizer.nutrients[1] > 0 and not FindFertilizer(inst, 0, 999, 999)) or
                    (item.components.fertilizer.nutrients[2] > 0 and not FindFertilizer(inst, 999, 0, 999)) or
                    (item.components.fertilizer.nutrients[3] > 0 and not FindFertilizer(inst, 999, 999, 0))) or
                    -- Seeds
                    (item.components.edible ~= nil and item.components.edible.foodtype == FOODTYPE.SEEDS and
                    (seedpouch == nil or item.prefab ~= "seeds"))
            end
        end)
    end
    return (target ~= nil and item ~= nil) and BufferedAction(inst, target, ACTIONS.LOAD, item) or nil
end

local TOPICKUP_CANT_TAGS = { "fire", "smolder", "INLIMBO", "NOCLICK", "event_trigger", "catchable", "irreplaceable", "heavy", "outofreach" }
function WXAgriculture:FindEntityToPickUpAction()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward") or self.inst

    local target = FindEntity(leader, SEE_WORK_DIST, function(item)
        return item ~= nil and
        item:IsValid() and
        not item:IsInLimbo() and
        item.entity:IsVisible() and
        item:IsOnValidGround() and
        item.components.inventoryitem ~= nil and
        item.components.inventoryitem.canbepickedup and
        --[[not (item.components.burnable ~= nil and
            (item.components.burnable:IsBurning() or
            item.components.burnable:IsSmoldering())) and]]
        -- Gardenhoe
        ((item.components.farmtiller ~= nil and not self.inst.components.wxtype:CanDoAction(ACTIONS.TILL, true)) or
        -- Shovel
        (item.components.tool ~= nil and
        item.components.tool:CanDoAction(ACTIONS.DIG) and
        not self.inst.components.wxtype:CanDoAction(ACTIONS.DIG, true)) or
        -- Hammer
        (item.components.tool ~= nil and
        item.components.tool:CanDoAction(ACTIONS.HAMMER) and
        not self.inst.components.wxtype:CanDoAction(ACTIONS.HAMMER, true)) or
        -- Wateringcan
        (not self.inst.components.wxtype:CanDoAction(ACTIONS.POUR_WATER_GROUNDTILE, true) and
        item:HasTag("wateringcan") and
        item.components.finiteuses ~= nil and (item.components.finiteuses:GetUses() > 0 or
        FindEntity(leader, SEE_WORK_DIST, function(ent) return ent.components.watersource ~= nil end))) or
        -- Seedpouch
        item.prefab == "seedpouch" or
        -- Fertilizer
        (item.components.fertilizer ~= nil and self.inst.components.wxstorage:ShouldPickUp(item) and
        ((item.components.fertilizer.nutrients[1] > 0 and not FindFertilizer(self.inst, 0, 999, 999)) or
        (item.components.fertilizer.nutrients[2] > 0 and not FindFertilizer(self.inst, 999, 0, 999)) or
        (item.components.fertilizer.nutrients[3] > 0 and not FindFertilizer(self.inst, 999, 999, 0)))) or
        -- Target item is seed or veggie
        (item.components.edible ~= nil and
        (item.components.edible.foodtype == FOODTYPE.SEEDS or
        item.components.edible.foodtype == FOODTYPE.VEGGIE)))
    end, nil, TOPICKUP_CANT_TAGS)

    local seedpouch = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    if seedpouch ~= nil and seedpouch.prefab ~= "seedpouch" then
        seedpouch = nil
    end
    if seedpouch == nil and target ~= nil and target.prefab == "seedpouch" then
        return BufferedAction(self.inst, target, ACTIONS.AUGMENT)
    end

    return (target ~= nil and target.components.container == nil) and
        BufferedAction(self.inst, target, ACTIONS.PICKUP) or
        FindItemToTakeAction(self.inst) or nil
end

return WXAgriculture