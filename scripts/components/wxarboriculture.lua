local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20

local WXArboriculture = Class(function(self, inst)
    self.inst = inst
end)

local xylogenList =
{
    "log",
    "pinecone",
    "acorn",
    "twigs",
    "twiggy_nut",
    "charcoal",
    "livinglog",
    --"moon_tree_blossom",
}

-- 5x5 tiles
local function GetPlantTurfsCoords(leader)
    local tile_x, tile_y = TheWorld.Map:GetTileCoordsAtPoint(leader.Transform:GetWorldPosition())
    local plantturfscoordsList = {}
    -- tilerange = (SEE_WORK_DIST - 2) % 4
    for i = -2, 2 do
        for j = -2, 2 do
            local tile = TheWorld.Map:GetTile(tile_x + i, tile_y + j)
            if tile ~= GROUND.FARMING_SOIL and
                tile ~= GROUND.ROCKY and
                tile ~= GROUND.ROAD and
                tile ~= GROUND.UNDERROCK and
                tile < GROUND.UNDERGROUND and
                tile ~= GROUND.IMPASSABLE and
                tile ~= GROUND.INVALID and
                not GROUND_FLOORING[tile] then
                table.insert(plantturfscoordsList, {tile_x = tile_x + i, tile_y = tile_y + j})
            end
        end
    end
    return plantturfscoordsList
end

local function GetPlantPoints(tile_x, tile_y)
    local tile = TheWorld.Map:GetTile(tile_x, tile_y)
    local x, y, z = TheWorld.Map:GetTileCenterPoint(tile_x, tile_y)
    local plantpointsList = {}
    local PLANTSEED_IGNORE_TAGS = { "wx" }
    local spacing = 1 -- 2/2
    for i = -1, 1, 2 do
        for j = -1, 1, 2 do
            if not next(TheSim:FindEntities(x + i * spacing, 0, z + j * spacing, 2, nil, PLANTSEED_IGNORE_TAGS)) then
                table.insert(plantpointsList, Vector3(x + i * spacing, 0, z + j * spacing))
            end
        end
    end
    return plantpointsList
end

local TOWORK_CANT_TAGS = { "fire", "smolder", "INLIMBO", "NOCLICK", "event_trigger" }
function WXArboriculture:Chop()
    local action = ACTIONS.CHOP
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local target = self.inst.sg.statemem.target
    if target ~= nil and
        target:IsValid() and
        not (target:IsInLimbo() or
            target:HasTag("NOCLICK") or
            target:HasTag("event_trigger")) and
        target:IsOnValidGround() and
        target.components.workable ~= nil and
        target.components.workable:CanBeWorked() and
        target.components.workable:GetWorkAction() == action and
        not (target.components.burnable ~= nil and
            (target.components.burnable:IsBurning() or
            target.components.burnable:IsSmoldering())) and
        target.entity:IsVisible() and
        target:IsNear(leader, KEEP_WORKING_DIST) then
        self.inst.components.wxtype:SwapTool(ACTIONS.CHOP)
        local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        return tool ~= nil and BufferedAction(self.inst, target, action, tool) or nil
    end

    target = FindEntity(leader, SEE_WORK_DIST, function(ent)
        if string.find(ent.prefab, "mushtree") or ent:HasTag("burnt") then
            return true
        elseif ent.components.growable ~= nil then
            return ent.components.growable:GetStage() > 1
        elseif ent.prefab == "cave_banana_tree" then
            return false
        end
    end, { action.id.."_workable" }, TOWORK_CANT_TAGS)
    if target ~= nil then
        self.inst.components.wxtype:SwapTool(ACTIONS.CHOP)
        local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        return tool ~= nil and BufferedAction(self.inst, target, action, tool) or nil
    end
end

local DIG_TAGS = { "stump", "grave" }
function WXArboriculture:DIG()
    local action = ACTIONS.DIG
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local target = self.inst.sg.statemem.target
    if target ~= nil and
        target:IsValid() and
        not (target:IsInLimbo() or
            target:HasTag("NOCLICK") or
            target:HasTag("event_trigger")) and
        target:IsOnValidGround() and
        target.components.workable ~= nil and
        target.components.workable:CanBeWorked() and
        target.components.workable:GetWorkAction() == action and
        not (target.components.burnable ~= nil and
            (target.components.burnable:IsBurning() or
            target.components.burnable:IsSmoldering())) and
        target.entity:IsVisible() and
        target:IsNear(leader, KEEP_WORKING_DIST) then

        for i, v in ipairs(DIG_TAGS) do
            if target:HasTag(v) then
                self.inst.components.wxtype:SwapTool(ACTIONS.DIG)
                local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
                return tool ~= nil and BufferedAction(self.inst, target, action, tool) or nil
            end
        end
    end

    target = FindEntity(leader, SEE_WORK_DIST, nil, { action.id.."_workable" }, TOWORK_CANT_TAGS, DIG_TAGS)
    if target ~= nil then
        self.inst.components.wxtype:SwapTool(ACTIONS.DIG)
        local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        return tool ~= nil and BufferedAction(self.inst, target, action, tool) or nil
    end
end

function WXArboriculture:PlantSeeds()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local seed = self.inst.components.inventory:FindItem(function(item)
        return item.components.deployable ~= nil and item.components.deployable.mode == DEPLOYMODE.PLANT
    end)

    if leader ~= nil and seed ~= nil then
        local plantturfscoordsList = GetPlantTurfsCoords(leader)
        if #plantturfscoordsList == 0 then
            return nil
        end

        for k, v in pairs(plantturfscoordsList) do
            local plantpointsList = GetPlantPoints(v.tile_x, v.tile_y)
            if #plantpointsList > 0 then
                local pt = plantpointsList[next(plantpointsList)]
                return BufferedAction(self.inst, nil, ACTIONS.DEPLOY, seed, pt)
            end
        end
    end
end

function WXArboriculture:StoreWood()
    local sentryward = self.inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    for k, xylogen in pairs(self.inst.components.inventory.itemslots) do
        if table.contains(xylogenList, xylogen.prefab) then
            -- Signed Chest
            local signedchest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and not ent.components.container:IsFull() and
                    FindEntity(ent, .5, function(sign)
                        return sign.components.drawable ~= nil and sign.components.drawable:GetImage() == xylogen.prefab
                    end)
            end)
            if signedchest ~= nil then
                return BufferedAction(self.inst, signedchest, ACTIONS.STORE, xylogen)
            end
            -- Unsigned Chest
            local unsignedchest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and
                    ent.components.container:Has(xylogen.prefab, 1) and
                    FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
            end)
            if unsignedchest ~= nil and not unsignedchest.components.container:IsFull() then
                return BufferedAction(self.inst, unsignedchest, ACTIONS.STORE, xylogen)
            end
            -- Empty Chest
            local emptychest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and ent.components.container:IsEmpty() and
                    FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
            end)
            if unsignedchest == nil and emptychest ~= nil then
                return BufferedAction(self.inst, emptychest, ACTIONS.STORE, xylogen)
            end
        end
    end
end

local function FindItemToTakeAction(inst)
    local sentryward = inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    local target = FindEntity(sentryward, SEE_WORK_DIST, function(chest)
        return chest.prefab == "treasurechest" and chest.components.container ~= nil and
            chest.components.container:FindItem(function(item)
                return item.components.tool ~= nil and
                    ((item.components.tool:CanDoAction(ACTIONS.CHOP) and
                    not inst.components.wxtype:CanDoAction(ACTIONS.CHOP, true)) or
                    (item.components.tool:CanDoAction(ACTIONS.DIG) and
                    not inst.components.wxtype:CanDoAction(ACTIONS.DIG, true)))
            end)
    end)
    local item = nil
    if target ~= nil then
        item = target.components.container:FindItem(function(item)
            return item.components.tool ~= nil and
                ((item.components.tool:CanDoAction(ACTIONS.CHOP) and
                not inst.components.wxtype:CanDoAction(ACTIONS.CHOP, true)) or
                (item.components.tool:CanDoAction(ACTIONS.DIG) and
                not inst.components.wxtype:CanDoAction(ACTIONS.DIG, true)))
        end)
    end
    return (target ~= nil and item ~= nil) and BufferedAction(inst, target, ACTIONS.LOAD, item) or nil
end

local TOPICKUP_CANT_TAGS = { "fire", "smolder", "INLIMBO", "NOCLICK", "event_trigger", "catchable", "irreplaceable", "heavy", "outofreach" }
function WXArboriculture:FindEntityToPickUpAction()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward") or self.inst

    local target = FindEntity(leader, SEE_WORK_DIST, function(item)
        return item ~= nil and
        item:IsValid() and
        not item:IsInLimbo() and
        item.entity:IsVisible() and
        item:IsOnValidGround() and
        item.components.inventoryitem ~= nil and
        item.components.inventoryitem.canbepickedup and
        --[[not (item.components.burnable ~= nil and
            (item.components.burnable:IsBurning() or
            item.components.burnable:IsSmoldering())) and]]
        -- Target item is axe or shovel
        ((item.components.tool ~= nil and
        ((item.components.tool:CanDoAction(ACTIONS.CHOP) and not self.inst.components.wxtype:CanDoAction(ACTIONS.CHOP, true)) or
        (item.components.tool:CanDoAction(ACTIONS.DIG) and not self.inst.components.wxtype:CanDoAction(ACTIONS.DIG, true)))) or
        -- Target item is xylogen
        (table.contains(xylogenList, item.prefab) and self.inst.components.wxstorage:ShouldPickUp(item)))
    end, nil, TOPICKUP_CANT_TAGS)

    return target ~= nil and BufferedAction(self.inst, target, ACTIONS.PICKUP) or FindItemToTakeAction(self.inst) or nil
end

return WXArboriculture