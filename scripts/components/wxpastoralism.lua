local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20

local WXPastoralism = Class(function(self, inst)
    self.inst = inst
end)

function WXPastoralism:Feed()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local beef = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.components.domesticatable ~= nil
    end)
    if beef == nil or beef.components.hunger:GetPercent() >
        (beef.components.domesticatable.domesticated and 0.8 or TUNING.BEEFALO_BEG_HUNGER_PERCENT) then
        return nil
    end
    local saltlick = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.prefab == "saltlick" and ent.components.finiteuses ~= nil and ent.components.finiteuses:GetUses() > 0
    end)
    local food = self.inst.components.inventory:FindItem(function(item)
        return item.components.edible ~= nil and
            (item.components.edible.foodtype == FOODTYPE.VEGGIE or
            item.components.edible.foodtype == FOODTYPE.ROUGHAGE)
    end)
    return (beef ~= nil and saltlick == nil and food ~= nil) and BufferedAction(self.inst, beef, ACTIONS.GIVE, food) or nil
end

function WXPastoralism:Brush()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local beef = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.components.brushable ~= nil
    end)
    if beef == nil or
        (beef.components.domesticatable and beef.components.domesticatable.domesticated) or
        beef.components.brushable:CalculateNumPrizes() < 1 then
        return nil
    end
    self.inst.components.wxtype:SwapTool(ACTIONS.BRUSH)
    local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    return (beef ~= nil and tool ~= nil) and BufferedAction(self.inst, beef, ACTIONS.BRUSH, tool) or nil
end

function WXPastoralism:Mount()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local beef = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.components.rideable ~= nil and
            ent.components.rideable:TestObedience() and
            ent.components.hunger:GetPercent() > 0.2
    end)
    if beef ~= nil and not beef.components.rideable:IsSaddled() then
        local saddle = self.inst.components.inventory:FindItem(function(item)
            return item.components.saddler ~= nil
        end)
        return saddle ~= nil and BufferedAction(self.inst, beef, ACTIONS.SADDLE, saddle) or nil
    elseif beef ~= nil then
        local player = FindEntity(beef, SEE_WORK_DIST / 2, function(ent)
            return ent:HasTag("player")
        end)
        return (player == nil and beef.components.rideable.canride) and BufferedAction(self.inst, beef, ACTIONS.MOUNT) or nil
    end
end

function WXPastoralism:Dismount()
    local player = FindEntity(self.inst, 4, function(ent)
        return ent:HasTag("player")
    end)
    if player ~= nil then
        return BufferedAction(self.inst, self.inst, ACTIONS.DISMOUNT)
    end

    local beef = self.inst.components.rider:GetMount()
    if not beef.components.rideable:TestObedience() or beef.components.hunger:GetPercent() < 0.1 then
        return BufferedAction(self.inst, self.inst, ACTIONS.DISMOUNT)
    end
end

local group = {{1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}, {0, 1}, {1, 1}}
local function GetNextDest(inst, sentryward)
    local x, y, z = inst.Transform:GetWorldPosition()
    local sentryward_x, sentryward_y, sentryward_z = sentryward.Transform:GetWorldPosition()
    local tile_x, tile_y = TheWorld.Map:GetTileCoordsAtPoint(x, y, z)
    local sentryward_tile_x, sentryward_tile_y = TheWorld.Map:GetTileCoordsAtPoint(sentryward_x, sentryward_y, sentryward_z)
    local tile_dx = tile_x - sentryward_tile_x
    local tile_dy = tile_y - sentryward_tile_y
    for k, v in pairs(group) do
        if tile_dx == v[1] and tile_dy == v[2] then
            return TheWorld.Map:GetTileCenterPoint(sentryward_tile_x + group[k+1][1], sentryward_tile_y + group[k+1][2])
        end
    end
    return TheWorld.Map:GetTileCenterPoint(sentryward_tile_x + 1, sentryward_tile_y + 1)
end

function WXPastoralism:WorkOut()
    local beef = self.inst.components.rider:GetMount()
    if beef == nil or
        beef.components.domesticatable.domesticated or
        not self.inst.components.rider:IsRiding() then
        return nil
    end

    local cane_in_inv = self.inst.components.inventory:FindItem(function(item)
        return item.prefab == "cane"
    end)
    local cane_on_hand = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    if cane_on_hand == nil or cane_on_hand.prefab ~= "cane" then
        self.inst.components.inventory:Equip(cane_in_inv)
    end
    cane_on_hand = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)

    if cane_on_hand ~= nil and cane_on_hand.prefab == "cane" then
        local landmark = self.inst.components.entitytracker:GetEntity("sentryward")
        if landmark ~= nil and not self.inst:IsNear(landmark, 2) then
            local pt = Vector3(GetNextDest(self.inst, landmark))
            return self.inst.components.locomotor:GoToPoint(pt, nil, true)
        end
    end
end

-- In case the saltlick becomes repairable some day
function WXPastoralism:Repair()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local beef = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.components.domesticatable ~= nil
    end)
    if beef ~= nil and beef.components.hunger > 0 or
        beef.components.domesticatable.domestication > 0.25 then
        return nil
    end
    local saltlick = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.prefab == "saltlick" and ent.components.repairable ~= nil
    end)
    local salt = self.inst.components.inventory:FindItem(function(item)
        return item.prefab == "salt" or item.prefab == "nitro"
    end)
    return (saltlick ~= nil and salt ~= nil) and BufferedAction(self.inst, saltlick, ACTIONS.REPAIR, salt)
end

function WXPastoralism:StoreFuel()
    local sentryward = self.inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    for k, product in pairs(self.inst.components.inventory.itemslots) do
        if product.components.fuel ~= nil and (product.components.edible == nil or
            product.components.edible.foodtype ~= FOODTYPE.VEGGIE and
            product.components.edible.foodtype ~= FOODTYPE.ROUGHAGE) then
            -- Signed Chest
            local signedchest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and not ent.components.container:IsFull() and
                    FindEntity(ent, .5, function(sign)
                        return sign.components.drawable ~= nil and sign.components.drawable:GetImage() == product.prefab
                    end)
            end)
            if signedchest ~= nil then
                return BufferedAction(self.inst, signedchest, ACTIONS.STORE, product)
            end
            -- Unsigned Chest
            local unsignedchest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and
                    ent.components.container:Has(product.prefab, 1) and
                    FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
            end)
            if unsignedchest ~= nil and not unsignedchest.components.container:IsFull() then
                return BufferedAction(self.inst, unsignedchest, ACTIONS.STORE, product)
            end
            -- Empty Chest
            local emptychest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and ent.components.container:IsEmpty() and
                    FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
            end)
            if unsignedchest == nil and emptychest ~= nil then
                return BufferedAction(self.inst, emptychest, ACTIONS.STORE, product)
            end
        end
    end
end

local function FindItemToTakeAction(inst)
    local sentryward = inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    local target = FindEntity(sentryward, SEE_WORK_DIST, function(chest)
        return (chest.prefab == "treasurechest" or chest.prefab == "icebox" or chest.prefab == "saltbox") and chest.components.container ~= nil and
            chest.components.container:FindItem(function(item)
                local cane = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
                return (item.components.brush ~= nil and not inst.components.wxtype:CanDoAction(ACTIONS.BRUSH, true)) or
                    --(item.components.unsaddler ~= nil and not inst.components.wxtype:CanDoAction(ACTIONS.UNSADDLE, true)) or
                    (inst.components.inventory:FindItem(function(item) return item.components.edible ~= nil end) == nil and
                    (item.components.edible ~= nil and
                    (item.components.edible.foodtype == FOODTYPE.VEGGIE or
                    item.components.edible.foodtype == FOODTYPE.ROUGHAGE))) or
                    (inst.components.inventory:FindItem(function(item) return item.components.saddler ~= nil end) == nil and
                    item.components.saddler ~= nil)
            end)
    end)
    local item = nil
    if target ~= nil then
        item = target.components.container:FindItem(function(item)
            local cane = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
            return (item.components.brush ~= nil and not inst.components.wxtype:CanDoAction(ACTIONS.BRUSH, true)) or
                --(item.components.unsaddler ~= nil and not inst.components.wxtype:CanDoAction(ACTIONS.UNSADDLE, true)) or
                inst.components.inventory:FindItem(function(item) return item.components.edible ~= nil end) == nil and
                (item.components.edible ~= nil and
                (item.components.edible.foodtype == FOODTYPE.VEGGIE or
                item.components.edible.foodtype == FOODTYPE.ROUGHAGE)) or
                (inst.components.inventory:FindItem(function(item) return item.components.saddler ~= nil end) == nil and
                item.components.saddler ~= nil)
        end)
    end
    return (target ~= nil and item ~= nil) and BufferedAction(inst, target, ACTIONS.LOAD, item) or nil
end

local TOPICKUP_CANT_TAGS = { "fire", "smolder", "INLIMBO", "NOCLICK", "event_trigger", "catchable", "irreplaceable", "heavy", "outofreach", "firefly" }
function WXPastoralism:FindEntityToPickUpAction()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward") or self.inst

    local cane = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    local target = FindEntity(leader, SEE_WORK_DIST, function(item)
        return item ~= nil and
        item:IsValid() and
        not item:IsInLimbo() and
        item.entity:IsVisible() and
        item:IsOnValidGround() and
        item.components.inventoryitem ~= nil and
        item.components.inventoryitem.canbepickedup and
        --[[not (item.components.burnable ~= nil and
            (item.components.burnable:IsBurning() or
            item.components.burnable:IsSmoldering())) and]]
        -- Target item is brush, saddlehorn or saddle
        ((item.components.brush ~= nil and not self.inst.components.wxtype:CanDoAction(ACTIONS.BRUSH, true)) or
        --(item.components.unsaddler ~= nil and not self.inst.components.wxtype:CanDoAction(ACTIONS.UNSADDLE, true)) or
        (self.inst.components.inventory:FindItem(function(item) return item.components.saddler ~= nil end) == nil and
        item.components.saddler ~= nil) or
        -- Target item is forage or fuel
        (item.components.edible ~= nil and
        (item.components.edible.foodtype == FOODTYPE.VEGGIE or
        item.components.edible.foodtype == FOODTYPE.ROUGHAGE)) or
        (item.components.fuel ~= nil and self.inst.components.wxstorage:ShouldPickUp(item)))
    end, nil, TOPICKUP_CANT_TAGS)

    return target ~= nil and BufferedAction(self.inst, target, ACTIONS.PICKUP) or FindItemToTakeAction(self.inst) or nil
end

return WXPastoralism