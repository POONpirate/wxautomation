--------------------------------------------------------------------------
--[[ Dependencies ]]
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--[[ WXNavigation class definition ]]
--------------------------------------------------------------------------
local WXNavigation = Class(function(self, inst)

--------------------------------------------------------------------------
--[[ Public Member Variables ]]
--------------------------------------------------------------------------
    self.inst = inst
    self.engaged = false
    self.navtarget = nil
    self.previousAP = nil
    self.pointqueue = {}

--------------------------------------------------------------------------
--[[ Private Member Variables ]]
--------------------------------------------------------------------------

end)

--------------------------------------------------------------------------
--[[ Private constants ]]
--------------------------------------------------------------------------
local CHEST_LOAD_DIST = 4
local CHEST_UNLOAD_DIST = 4

local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20

--------------------------------------------------------------------------
--[[ Private member functions ]]
--------------------------------------------------------------------------
local function IsWChest(inst)
    return inst.prefab == "treasurechest" and
        inst.components.container ~= nil and
        not inst.components.container:IsFull() and
        FindEntity(inst, SEE_WORK_DIST, function(ent) return ent.prefab == "wxdiviningrodbase" end)
end

local function IsWIcebox(inst)
    return inst.prefab == "icebox" and
        inst.components.container ~= nil and
        not inst.components.container:IsFull() and
        FindEntity(inst, SEE_WORK_DIST, function(ent) return ent.prefab == "wxdiviningrodbase" end)
end

local function IsWSaltbox(inst)
    return inst.prefab == "saltbox" and
        inst.components.container ~= nil and
        not inst.components.container:IsFull() and
        FindEntity(inst, SEE_WORK_DIST, function(ent) return ent.prefab == "wxdiviningrodbase" end)
end

local function IsAPContainer(inst)
    return (inst.prefab == "treasurechest" or inst.prefab == "icebox" or inst.prefab == "saltbox") and
        inst.components.container ~= nil and
        not inst.components.container:IsEmpty() and
        FindEntity(inst, SEE_WORK_DIST, function(ent) return ent.prefab == "sentryward" end) and
        FindEntity(inst, SEE_WORK_DIST, function(ent) return ent.prefab == "wxdiviningrodbase" end) == nil
end

-- GetClosest() is a build-in function in util.lua
local function GetScheduled(inst, List)
    local ent = nil
    local maxweight = 0
    local nowtime = TheWorld.components.worldstate.data.cycles
    for k, v in pairs(List) do
        if v.ent:IsValid() and not v.ent:IsInLimbo() then
            -- Release invalid engagement
            if v.state ~= nil and (not v.state:IsValid() or v.state:IsInLimbo() or
                not v.state.components.wxtype:IsConv() or inst.components.follower.leader ~= nil) then
                v.state = nil
            end
            -- Find best choice
            local newweight = (nowtime - v.lasttime) * v.load
            if v.state == nil and newweight >= maxweight then
                ent = v.ent
                maxweight = newweight
            end
        else
            table.remove(List, k)
        end
    end
    for k, v in pairs(List) do
        -- Lock the choice
        if ent == v.ent then
            v.state = inst
        end
    end
    return ent
end

--------------------------------------------------------------------------
--[[ Private event handlers ]]
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--[[ Public member functions ]]
--------------------------------------------------------------------------
function WXNavigation:UnloadCargo()
    local wxdiviningrodbase = FindEntity(self.inst, KEEP_WORKING_DIST * 2, function(ent) return ent.prefab == "wxdiviningrodbase" end)
    if wxdiviningrodbase == nil then
        return nil
    end

    local saltbox = FindEntity(self.inst, SEE_WORK_DIST, function(ent) return IsWSaltbox(ent) end)
    local icebox = FindEntity(self.inst, SEE_WORK_DIST, function(ent) return IsWIcebox(ent) end)
    local treasurechest = FindEntity(self.inst, SEE_WORK_DIST, function(ent) return IsWChest(ent) end)
    if saltbox == nil and icebox == nil and treasurechest == nil then
        return nil
    end

    self.pointqueue = {}

    for k, v in pairs(TheWorld.sentryward) do
        if v.ent == self.previousAP then
            v.lasttime = TheWorld.components.worldstate.data.cycles
        end
    end

    local backpack = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    local item = nil
    if backpack ~= nil and backpack.components.container ~= nil and not backpack.components.container:IsEmpty() then
        item = backpack.components.container:FindItem(function(item) return true end)
    elseif next(self.inst.components.inventory.itemslots) ~= nil then
        item = self.inst.components.inventory:FindItem(function(item) return true end)
    end

    if saltbox ~= nil and item ~= nil and
        item.components.edible ~= nil and item.components.edible.foodtype == FOODTYPE.VEGGIE and
        item.components.cookable ~= nil and item:HasTag("cookable") then
        return BufferedAction(self.inst, saltbox, ACTIONS.STORE, item)
    elseif icebox ~= nil and item ~= nil and
        item.components.edible ~= nil and item.components.perishable ~= nil then
        return BufferedAction(self.inst, icebox, ACTIONS.STORE, item)
    elseif treasurechest ~= nil and item ~= nil then
        -- Signed Chest
        local signedchest = FindEntity(wxdiviningrodbase, SEE_WORK_DIST, function(ent)
            return ent.prefab == "treasurechest" and ent.components.container ~= nil and not ent.components.container:IsFull() and
                FindEntity(ent, .5, function(sign)
                    return sign.components.drawable ~= nil and sign.components.drawable:GetImage() == item.prefab
                end)
        end)
        if signedchest ~= nil then
            return BufferedAction(self.inst, signedchest, ACTIONS.STORE, item)
        end
        -- Unsigned Chest
        local unsignedchest = FindEntity(wxdiviningrodbase, SEE_WORK_DIST, function(ent)
            return ent.prefab == "treasurechest" and ent.components.container ~= nil and
                ent.components.container:Has(item.prefab, 1) and
                FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
        end)
        if unsignedchest ~= nil and not unsignedchest.components.container:IsFull() then
            return BufferedAction(self.inst, unsignedchest, ACTIONS.STORE, item)
        end
        -- Empty Chest
        local emptychest = FindEntity(wxdiviningrodbase, SEE_WORK_DIST, function(ent)
            return ent.prefab == "treasurechest" and ent.components.container ~= nil and ent.components.container:IsEmpty() and
                FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
        end)
        if unsignedchest == nil and emptychest ~= nil then
            return BufferedAction(self.inst, emptychest, ACTIONS.STORE, item)
        end
        --return BufferedAction(self.inst, treasurechest, ACTIONS.STORE, item)
    end
end

function WXNavigation:LoadCargo()
    local container = FindEntity(self.inst, SEE_WORK_DIST, function(ent) return IsAPContainer(ent) end)
    if container == nil then
        return
    end

    self.pointqueue = {}

    local sentryward = FindEntity(self.inst, SEE_WORK_DIST, function(ent)
        return ent.prefab == "sentryward"
    end)
    local backpack = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    for k, v in pairs(TheWorld.sentryward) do
        if v.ent == sentryward then
            v.load = #self.inst.components.inventory.itemslots + 1
            if backpack ~= nil and backpack.components.container ~= nil then
                v.load = v.load + #backpack.components.container.slots
            end
        end
    end

    local item = container.components.container:FindItem(function(item) return true end)
    return item ~= nil and BufferedAction(self.inst, container, ACTIONS.LOAD, item) or nil
end

function WXNavigation:AutoNavigate(toAP)
    if self.engaged then
        -- Teleporting WX doesn't need a hiking list.
        local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        if tool ~= nil and tool.prefab == "compass" then
            self.pointqueue = {}
            return
        end
        -- Hiking WX goes to the next hiking spot.
        if next(self.pointqueue) == nil then
            if self.inst.searching_task == nil then
                self.inst.searching_task = self.inst:DoTaskInTime(10, function(inst)
                    self.inst.components.talker:Say("WARNING: CANNOT FIND THE PATH SPECIFIED")
                    if inst.searching_task ~= nil then
                        inst.searching_task:Cancel()
                        inst.searching_task = nil
                    end
                end)
                return
            end
            for k, v in pairs(TheWorld.sentryward) do
                if self.navtarget ~= nil and v.ent == self.navtarget then
                    v.state = nil
                end
            end
            self.engaged = false
        else
            if self.inst.searching_task ~= nil then
                self.inst.searching_task:Cancel()
                self.inst.searching_task = nil
                self.inst.components.talker:Say("PATH DETECTED, DEPARTURE AUTHORIZED")
            end

            local succeedingpoint = self.pointqueue[#self.pointqueue]
            local wxtile_x, wxtile_y = TheWorld.Map:GetTileCoordsAtPoint(self.inst.Transform:GetWorldPosition())
            if math.abs(succeedingpoint.x - wxtile_x) <= 1 and math.abs(succeedingpoint.y - wxtile_y) <= 1 then
                table.remove(self.pointqueue)
                return
            end
            succeedingpoint = self.pointqueue[#self.pointqueue]
            local pt = Vector3(TheWorld.Map:GetTileCenterPoint(succeedingpoint.x, succeedingpoint.y))
            self.inst.components.locomotor:GoToPoint(pt, nil, true)
        end
        return
    end

    if toAP then
        self.navtarget = GetScheduled(self.inst, TheWorld.sentryward)
        -- Release previous AP
        for k, v in pairs(TheWorld.sentryward) do
            if self.previousAP ~= nil and v.ent == self.previousAP and v.ent ~= self.navtarget and
                v.state == self.inst then
                v.state = nil
            end
        end
        self.previousAP = self.navtarget
    else
        local wxdiviningrodbaseList = {}
        for k, v in pairs(TheWorld.wxdiviningrodbase) do
            if v.components.shelf.itemonshelf ~= nil then
                table.insert(wxdiviningrodbaseList, v)
            end
        end
        self.navtarget = GetClosest(self.inst, wxdiviningrodbaseList)
    end

    local pt = self.navtarget and self.navtarget:GetPosition() or nil
    if pt == nil then
        return
    end
    self.engaged = true

    local theta = math.random() * 2 * PI
    local radius = math.random(0, 4)
    local result_offset = FindValidPositionByFan(theta, radius, 12, function(offset)
        local pos = pt + offset
        local ents = TheSim:FindEntities(pos.x, 0, pos.z, 1)
        return not next(ents) and TheWorld.Map:IsPassableAtPoint(pos:Get())
    end)

    -- Has a navtarget
    if result_offset ~= nil then
        if self.inst.components.talker.task == nil and self.inst.searching_task == nil then
            self.inst.components.talker:Say("NAVIGATION SYSTEM ONLINE")
        end

        -- Hyper space jump
        local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        if tool ~= nil and tool.prefab == "compass" then
            -- Depart
            self.inst.components.talker:Say("HYPER SPACE JUMP IMMINENTS")
            self.inst:DoTaskInTime(3, function(inst)
                local fx = SpawnPrefab("spawn_fx_medium")
                fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
                inst:DoTaskInTime(.25, function(inst)
                    inst.Transform:SetPosition(0, 0, 0)
                end)
            end)
            -- Arrive
            self.inst:DoTaskInTime(6 + math.random() * 2, function(inst)
                local fx = SpawnPrefab("spawn_fx_medium")
                fx.Transform:SetPosition((pt + result_offset):Get())
                inst:DoTaskInTime(.25, function(inst)
                    inst.Transform:SetPosition((pt + result_offset):Get())
                    inst.components.talker:Say("LANDING SUCCESS")
                    inst.components.wxnavigation.engaged = false
                end)
            end)
        -- Hike
        else
            local start_x, start_y = TheWorld.Map:GetTileCoordsAtPoint(self.inst.Transform:GetWorldPosition())
            local dest_x, dest_y = TheWorld.Map:GetTileCoordsAtPoint((pt + result_offset):Get())
            self.inst.components.astarpathfinding:GetPath(start_x, start_y, dest_x, dest_y)
        end
    -- Has no valid navtarget
    else
        if self.inst.components.talker.task == nil and self.inst.searching_task == nil then
            self.inst.components.talker:Say("WARNING: NO VALID LANDING AREA FOUND")
        end
        return nil
    end
end

local TOPICKUP_CANT_TAGS = { "fire", "smolder", "INLIMBO", "NOCLICK", "event_trigger", "catchable", "irreplaceable", "heavy", "outofreach" }
function WXNavigation:FindEntityToPickUpAction()
    local backpack = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    local target = FindEntity(self.inst, SEE_WORK_DIST, function(item)
        return item ~= nil and
        item:IsValid() and
        not item:IsInLimbo() and
        item.entity:IsVisible() and
        item:IsOnValidGround() and
        item.components.inventoryitem ~= nil and
        item.components.inventoryitem.canbepickedup and
        --[[not (item.components.burnable ~= nil and
            (item.components.burnable:IsBurning() or
            item.components.burnable:IsSmoldering())) and]]
        -- Target item is backpack
        (backpack == nil and item.components.equippable ~= nil and item.components.container ~= nil and
        item.prefab ~= "seedpouch" and item.prefab ~= "candybag")
    end, nil, TOPICKUP_CANT_TAGS)

    return (backpack == nil and target ~= nil) and BufferedAction(self.inst, target, ACTIONS.AUGMENT) or nil
end

--------------------------------------------------------------------------
--[[ Save/Load ]]
--------------------------------------------------------------------------

--------------------------------------------------------------------------
--[[ Initialization ]]
--------------------------------------------------------------------------

return WXNavigation