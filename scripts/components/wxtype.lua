local WXType = Class(function(self, inst)
    self.inst = inst
end)

local function ToolCanDoAction(tool, action)
    if tool ~= nil then
        if tool.components.tool ~= nil then
            return tool.components.tool:CanDoAction(action)
        elseif action == ACTIONS.TILL then
            return tool.components.farmtiller ~= nil
        elseif action == ACTIONS.POUR_WATER or action == ACTIONS.POUR_WATER_GROUNDTILE then
            return tool:HasTag("wateringcan")
        elseif action == ACTIONS.FISH then
            return tool.components.fishingrod ~= nil
            elseif action == ACTIONS.OCEAN_FISHING_CAST then
            return tool.components.oceanfishingrod ~= nil
        elseif action == ACTIONS.BRUSH then
            return tool.components.brush ~= nil
        elseif action == ACTIONS.UNSADDLE then
            return tool.components.unsaddler ~= nil
        end
    end
end

function WXType:CanDoAction(action, CHECKITEMSLOTS)
    for k, v in pairs(self.inst.components.inventory.equipslots) do
        if v and ToolCanDoAction(v, action) then
            return v
        end
    end
    if CHECKITEMSLOTS == nil then
        return
    end
    for k, v in pairs(self.inst.components.inventory.itemslots) do
        if v and ToolCanDoAction(v, action) then
            return v
        end
    end
end

function WXType:SwapTool(action)
    local tool_in_inv = self.inst.components.inventory:FindItem(function(item)
        return ToolCanDoAction(item, action)
    end)
    local tool_on_hand = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    if tool_in_inv ~= nil and not ToolCanDoAction(tool_on_hand, action) then
        self.inst.components.inventory:Equip(tool_in_inv)
    end
end

function WXType:IsMili()
    local helmet = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    local armor = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    local weapon = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    return (helmet ~= nil and helmet.components.armor ~= nil) or (armor ~= nil and armor.components.armor ~= nil) or
        (weapon ~= nil and weapon.components.weapon ~= nil and weapon.components.tool == nil and
        FunctionOrValue(weapon.components.weapon:GetDamage(self.inst, self.inst)) >= TUNING.NIGHTSTICK_DAMAGE) or nil
end

function WXType:IsConv()
    local hat = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    return hat ~= nil and hat.prefab == "deserthat" or nil
end

function WXType:IsAgri()
    local hat = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    return hat ~= nil and hat:HasTag("plantinspector") or nil
end

function WXType:IsHorti()
    local hat = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    return hat ~= nil and hat.prefab == "strawhat" or nil
end

function WXType:IsArbori()
    local hat = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    return hat ~= nil and hat.prefab == "catcoonhat" or nil
end

function WXType:IsApi()
    local hat = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    if hat ~= nil and hat.prefab == "beehat" then
        self.inst:AddTag("insect")
        return true
    else
        self.inst:RemoveTag("insect")
        return nil
    end
end

function WXType:IsAqua()
    local hat = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    local coat = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.BODY)
    return (hat ~= nil and coat ~= nil) and (hat.prefab == "rainhat" and coat.prefab == "raincoat") or nil
end

function WXType:IsMiningInd()
    local hat = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    return hat ~= nil and hat.prefab == "minerhat" or nil
end

function WXType:IsPast()
    local hat = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
    return hat ~= nil and hat.prefab == "beefalohat" or nil
end

return WXType