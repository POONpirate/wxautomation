local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20

local WXHorticulture = Class(function(self, inst)
    self.inst = inst
end)

local TOWORK_CANT_TAGS = { "fire", "smolder", "INLIMBO", "NOCLICK", "event_trigger" }
function WXHorticulture:Fertilize()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local plant = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.components.pickable ~= nil and ent.components.pickable:CanBeFertilized()
    end, nil, TOWORK_CANT_TAGS)
    local fertilizer = self.inst.components.inventory:FindItem(function(item)
        return item.components.fertilizer ~= nil
    end)
    return (plant ~= nil and fertilizer ~= nil) and BufferedAction(self.inst, plant, ACTIONS.FERTILIZE, fertilizer) or nil
end

function WXHorticulture:HarvestFruits()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local plant = FindEntity(leader, SEE_WORK_DIST, function(ent)
        return ent.components.pickable ~= nil and ent.components.pickable:CanBePicked() and ent.components.pickable.caninteractwith ~= false
    end, nil, TOWORK_CANT_TAGS)
    return plant ~= nil and self.inst.components.wxstorage:ShouldHarvest(plant) and BufferedAction(self.inst, plant, ACTIONS.PICK) or nil
end

function WXHorticulture:StoreVeggies()
    local sentryward = self.inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    for k, other_product in pairs(self.inst.components.inventory.itemslots) do
        if other_product.components.perishable == nil and other_product.components.fertilizer == nil then
            -- Signed Chest
            local signedchest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and not ent.components.container:IsFull() and
                    FindEntity(ent, .5, function(sign)
                        return sign.components.drawable ~= nil and sign.components.drawable:GetImage() == other_product.prefab
                    end)
            end)
            if signedchest ~= nil then
                return BufferedAction(self.inst, signedchest, ACTIONS.STORE, other_product)
            end
            -- Unsigned Chest
            local unsignedchest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and
                    ent.components.container:Has(other_product.prefab, 1) and
                    FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
            end)
            if unsignedchest ~= nil and not unsignedchest.components.container:IsFull() then
                return BufferedAction(self.inst, unsignedchest, ACTIONS.STORE, other_product)
            end
            -- Empty Chest
            local emptychest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and ent.components.container:IsEmpty() and
                    FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
            end)
            if unsignedchest == nil and emptychest ~= nil then
                return BufferedAction(self.inst, emptychest, ACTIONS.STORE, other_product)
            end
        end
    end

    local saltbox = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
        return ent.prefab == "saltbox" and ent.components.container ~= nil and not ent.components.container:IsFull()
    end)
    local icebox = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
        return ent.prefab == "icebox" and ent.components.container ~= nil and not ent.components.container:IsFull()
    end)

    if saltbox ~= nil then
        local raw_food = self.inst.components.inventory:FindItem(function(item)
            return item.components.edible ~= nil and item.components.edible.foodtype == FOODTYPE.VEGGIE and
                item.components.cookable ~= nil and item:HasTag("cookable")
        end)
        return raw_food ~= nil and BufferedAction(self.inst, saltbox, ACTIONS.STORE, raw_food) or nil
    elseif icebox ~= nil then
        local any_food = self.inst.components.inventory:FindItem(function(item)
            return item.components.edible ~= nil and item.components.perishable ~= nil
        end)
        return any_food ~= nil and BufferedAction(self.inst, icebox, ACTIONS.STORE, any_food) or nil
    end
end

local function FindItemToTakeAction(inst)
    local sentryward = inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    local target = FindEntity(sentryward, SEE_WORK_DIST, function(chest)
        return chest.prefab == "treasurechest" and chest.components.container ~= nil and
            chest.components.container:FindItem(function(item)
                return item.components.fertilizer ~= nil and
                    inst.components.inventory:FindItem(function(item)
                        return item.components.fertilizer ~= nil
                    end) == nil
            end)
    end)
    local item = nil
    if target ~= nil then
        item = target.components.container:FindItem(function(item)
            return item.components.fertilizer ~= nil
        end)
    end
    return (target ~= nil and item ~= nil) and BufferedAction(inst, target, ACTIONS.LOAD, item) or nil
end

local TOPICKUP_CANT_TAGS = { "fire", "smolder", "INLIMBO", "NOCLICK", "event_trigger", "catchable", "irreplaceable", "heavy", "outofreach" }
function WXHorticulture:FindEntityToPickUpAction()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward") or self.inst

    local target = FindEntity(leader, SEE_WORK_DIST, function(item)
        return item ~= nil and
        item:IsValid() and
        not item:IsInLimbo() and
        item.entity:IsVisible() and
        item:IsOnValidGround() and
        item.components.inventoryitem ~= nil and
        item.components.inventoryitem.canbepickedup and
        --[[not (item.components.burnable ~= nil and
            (item.components.burnable:IsBurning() or
            item.components.burnable:IsSmoldering())) and]]
        -- Target item is fertilizer, grass, twigs or veggie
        (((item.prefab == "cutgrass" or item.prefab == "twigs" or item.components.fertilizer ~= nil) and
        self.inst.components.wxstorage:ShouldPickUp(item)) or
        (item.components.edible ~= nil and item.components.edible.foodtype == FOODTYPE.VEGGIE)
        )
    end, nil, TOPICKUP_CANT_TAGS)

    return target ~= nil and BufferedAction(self.inst, target, ACTIONS.PICKUP) or FindItemToTakeAction(self.inst) or nil
end

return WXHorticulture