local SEE_WORK_DIST = 14
local KEEP_WORKING_DIST = 20

local WXMiningIndustry = Class(function(self, inst)
    self.inst = inst
end)

local mineralList =
{
    "rocks",
    "flint",
    "goldnugget",
    "nitre",
    "ice",
    "marble",
    "thulecite_pieces",
    "thulecite",
    "moonrocknugget",
    "moonglass",
    "saltrock",
    --gems
}

local TOWORK_CANT_TAGS = { "fire", "smolder", "INLIMBO", "NOCLICK", "event_trigger" }
function WXMiningIndustry:Mine()
    local action = ACTIONS.MINE
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward")
    if leader == nil then
        return nil
    end

    local target = self.inst.sg.statemem.target
    if target ~= nil and
        target:IsValid() and
        not (target:IsInLimbo() or
            target:HasTag("NOCLICK") or
            target:HasTag("event_trigger")) and
        target:IsOnValidGround() and
        target.components.workable ~= nil and
        target.components.workable:CanBeWorked() and
        target.components.workable:GetWorkAction() == action and
        not (target.components.burnable ~= nil and
            (target.components.burnable:IsBurning() or
            target.components.burnable:IsSmoldering())) and
        target.entity:IsVisible() and
        target:IsNear(leader, KEEP_WORKING_DIST) then
        self.inst.components.wxtype:SwapTool(ACTIONS.MINE)
        local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        return tool ~= nil and BufferedAction(self.inst, target, action, tool) or nil
    end

    target = FindEntity(leader, SEE_WORK_DIST, function(rock)
        if rock.prefab == "rock_ice" and leader.prefab == "sentryward" then
            local icebox = FindEntity(leader, SEE_WORK_DIST, function(ent)
                return ent.prefab == "icebox" and not ent.components.container:IsFull()
            end)
            return icebox ~= nil
        else
            return true
        end
    end, { action.id.."_workable" }, TOWORK_CANT_TAGS)
    if target ~= nil then
        self.inst.components.wxtype:SwapTool(ACTIONS.MINE)
        local tool = self.inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
        return tool ~= nil and BufferedAction(self.inst, target, action, tool) or nil
    end
end

function WXMiningIndustry:StoreMineral()
    local sentryward = self.inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    local icebox = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
        return ent.prefab == "icebox" and ent.components.container ~= nil and not ent.components.container:IsFull()
    end)
    local ice = self.inst.components.inventory:FindItem(function(item)
        return item.prefab == "ice"
    end)
    if icebox ~= nil and ice ~= nil then
        return BufferedAction(self.inst, icebox, ACTIONS.STORE, ice)
    end

    for k, mineral in pairs(self.inst.components.inventory.itemslots) do
        if table.contains(mineralList, mineral.prefab) then
            -- Signed Chest
            local signedchest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and not ent.components.container:IsFull() and
                    FindEntity(ent, .5, function(sign)
                        return sign.components.drawable ~= nil and sign.components.drawable:GetImage() == mineral.prefab
                    end)
            end)
            if signedchest ~= nil then
                return BufferedAction(self.inst, signedchest, ACTIONS.STORE, mineral)
            end
            -- Unsigned Chest
            local unsignedchest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and
                    ent.components.container:Has(mineral.prefab, 1) and
                    FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
            end)
            if unsignedchest ~= nil and not unsignedchest.components.container:IsFull() then
                return BufferedAction(self.inst, unsignedchest, ACTIONS.STORE, mineral)
            end
            -- Empty Chest
            local emptychest = FindEntity(sentryward, SEE_WORK_DIST, function(ent)
                return ent.prefab == "treasurechest" and ent.components.container ~= nil and ent.components.container:IsEmpty() and
                    FindEntity(ent, .5, function(sign) return sign.components.drawable ~= nil end) == nil
            end)
            if unsignedchest == nil and emptychest ~= nil then
                return BufferedAction(self.inst, emptychest, ACTIONS.STORE, mineral)
            end
        end
    end
end

local function FindItemToTakeAction(inst)
    local sentryward = inst.components.entitytracker:GetEntity("sentryward")
    if sentryward == nil then
        return nil
    end

    local target = FindEntity(sentryward, SEE_WORK_DIST, function(chest)
        return chest.prefab == "treasurechest" and chest.components.container ~= nil and
            chest.components.container:FindItem(function(item)
                return item.components.tool ~= nil and item.components.tool:CanDoAction(ACTIONS.MINE) and
                    not inst.components.wxtype:CanDoAction(ACTIONS.MINE, true)
            end)
    end)
    local item = nil
    if target ~= nil then
        item = target.components.container:FindItem(function(item)
            return item.components.tool ~= nil and item.components.tool:CanDoAction(ACTIONS.MINE)
        end)
    end
    return (target ~= nil and item ~= nil) and BufferedAction(inst, target, ACTIONS.LOAD, item) or nil
end

local TOPICKUP_CANT_TAGS = { "INLIMBO", "NOCLICK", "event_trigger", "catchable", "irreplaceable", "heavy", "outofreach" }
function WXMiningIndustry:FindEntityToPickUpAction()
    local leader = self.inst.components.follower.leader or self.inst.components.entitytracker:GetEntity("sentryward") or self.inst

    local target = FindEntity(leader, SEE_WORK_DIST, function(item)
        return item ~= nil and
        item:IsValid() and
        not item:IsInLimbo() and
        item.entity:IsVisible() and
        item:IsOnValidGround() and
        item.components.inventoryitem ~= nil and
        item.components.inventoryitem.canbepickedup and
        --[[not (item.components.burnable ~= nil and
            (item.components.burnable:IsBurning() or
            item.components.burnable:IsSmoldering())) and]]
        -- Target item is pickaxe
        ((item.components.tool ~= nil and item.components.tool:CanDoAction(ACTIONS.MINE) and
        not self.inst.components.wxtype:CanDoAction(ACTIONS.MINE, true)) or
        -- Target item is mineral
        (table.contains(mineralList, item.prefab) and self.inst.components.wxstorage:ShouldPickUp(item)))
    end, nil, TOPICKUP_CANT_TAGS)

    return target ~= nil and BufferedAction(self.inst, target, ACTIONS.PICKUP) or FindItemToTakeAction(self.inst) or nil
end

return WXMiningIndustry