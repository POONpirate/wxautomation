-- Mod Name
name = "WX Automation"

-- Mod Description
description = 
"This mod allows player to construct WX automatons. "..
"The WXs will do work automatically for player, "..
"greatly saves time on farming and logging, "..
"and get solo players out of grinding. "..
"WX-78 can resurrect from the WX automaton."

-- Mod Author
author = "XueYanYu"

-- Mod Version
version = "1.4.1"

-- Klei Forum Thread
forumthread = ""

-- Mod API Version
api_version = 10

-- Mod Compatibility
dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false
dst_compatible = true

-- All client require this mod
all_clients_require_mod = true

-- Icon File
icon_atlas = "modicon.xml"
icon = "modicon.tex"

-- Configuration
configuration_options = {
    {
        name = "BUILD_RESTRICTION",
        hover = "Which characters can build the robots?",
        label = "Build Restriction",
        options = {
            {description = "All", data = "all"},
            {description = "Just WX-78", data = "wx"},
            {description = "And Engineers", data = "engineers"},
            {description = "And Scientists", data = "scientists"},
        },
        default = "all"
    },
}